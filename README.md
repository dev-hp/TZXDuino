# TZXDuino

## Principle of Operation

An Arduino Uno or Nano with an SD card interface to read files, a few buttons for input, an LCD or OLED for output, and an audio amplifier are the hardware base for TZXDuino.

You select a file from the SD card, and the Arduino program converts this into the correct impulses for a homecomputer (or emulator) to read, as if it came from a real cassette tape player.

The core of the device is the tone (pulse) generation. This task is split into two - an `Interrupt Service Routine` (ISR) to read period lengths from a buffer and transform this into LOW and HIGH pulses with a defined length, and a `TZXLoop` part that reads data from the SD card, parses it and fills the buffer for the ISR.

### Interrupt Service Routine (ISR)

The original TZXDuino Interrupt Service Routine not only read data from one of two work buffers, but also processed the level changes, made an exemption for UEF and AY data, and implemented the correct 'pause' behaviour - leave the current output signal state as it is for 1.5 milliseconds, then switch to LOW for the rest of the pause. It also processed the polarity change required for the 'Gremlin loader' (pause levels are HIGH instead of LOW).

The modified/refactored ISR is much simpler:

- First it reads a flag (bIsRunning).
- If bIsRunning is `false`, it just sets a period of 100ms (for the time until the next interrupt occurs) and returns.
- if bIsRunning is `true`, it takes a `workingPeriod` value from the waveBuffer.
  - If `workingPeriod` is 0, the ISR sets a 1ms period and returns.
  - If `workingPeriod` is not 0, it separates bit15 and bit14 from the rest:
    - bit15 signals a pause,
    - bit14 is the output level (HIGH or LOW),
    - bit13..0 are the period value.
    - Then it advances the wavePos pointer (and makes sure it stays inside the buffer)
- Finally the ISR sets the new period value for the next Interrupt.

This means that bIsRunning=false or an empty waveBuffer let the ISR pause. It does not touch anything, the wavePos pointer is not incremented, the audio output bit is not changed.

And: the waveISR routine never writes into the waveBuffer. It does not need to, all processing is done by TZXLoop for it.

### Pause values in the ISR

For a pause, the TZX-file-provided period values need to be multiplied with 1000 - pause periods are in milliseconds (ms) not microseconds (us). The ISR gets a value it multiplies with 1024.

- Multiplication by 1024 is much quicker than multiplication with 1000 - it is just shifting the value to the left by 10 bits.

The IDPAUSE routine in TZXLoop that generates the pause data in the waveBuffer corrects the pause value.

- It multiplies the input value read from the file on SD card by 1000,
- it adds 512 (for proper rounding in the next step)
- and subtracts a fixed period of 1500 microseconds (for the 'keep last output level active for at least 1ms' from the TZX format standard).
- Then it shifts the result 10 bits to the right, so it comfortably fits into a `word` datatype. Finally it sets the 'pause' indicator, bit 15. (This explanation is a bit simplified - in fact IDPAUSE is run two times - the first time it makes that 1500 microsecond hold-last-value entry, then on the second time it feberates the multi-milliseconds pause.)

### Double-buffering

The original code used an explicit 'double buffer' - not a bad choice, but it is possible to achieve the same behavior with a simpler data structure, and simpler computations, saving a bit of code and data.

- A double buffer of 64 words can be represented as one buffer of 128 words. We need to track two positions - the ISR 'wavPos' pointer for reading data from the buffer (it never writes), and the TZXLoop 'fillPos' pointer for writing the processed period values into the buffer.
- Both pointers run from 0 to 127, we can use a bit mask of 0x7F (decimal 127) to 'wrap' the position to stay within the buffer.
- As long as bit 6 (0x40 or decimal 64) of the two pointers is different, the pointers are in separate halves of the buffer.
- If they are identical, both pointers are in the same half of the buffer and TZXLoop stops filling the buffer, until the high bits are different again.
- This means that fillPos will normally stop at position 0 and at position 64 (until the ISR has caught up).

### TZXLoop

### A word about MUTEXes

The original code used 'noInterrupts();' and 'Interrupts();' statements to protect the waveBuffer while writing to it - TZXLoop wrote the computed period values into it, but also the ISR wrote back changed buffer values to track its pause processing.

On an ESP32, you don't stop all interrupts, you define a 'MUTEX' a `mut`ually `ex`clusive semaphor, that protects an area of code (between 'Enter' and 'Exit' statements) from being executed in parallel. But function and intent are the same - make sure ISR and TZXLoop do not write and read at nearly the same time from the same buffer content.

If you can make sure that the two parts of a program never both write to a resource, but one of them only writes and one of them only reads the resource, and if the resource can be written 'atomically', then you don't need a MUTEX or to disable interrupts. All variables we use meet this criterium. The waveBuffer is protected by the 'double buffer' construct, and that relies on two variables that are a single byte each (and both positions are also only read from here).

### A word about Overrun Errors

Initially, I thought porting to the ODROID-GO would be very simple - after all the hardware behind the Arduino core is much more powerful than an AVR core.

So when I listened to the first tape output - and that just sounded 'wrong', and severely delayed, initially I feared that the root cause for this was a problem with the interrupt processing, or the timer setting of the period until the next call of the ISR.

After quite some trials, Duncan sent me a link to an experimental ESP32CAS version - CASDuino for ESP32. And that worked well with interrupts. And there is also a version with OLED or LCD output. So the SPI interface is also not the problem.

Finally I found it. ODROID-GO has a 'battery protection' feature that I had enabled. It measures the voltage of the built-in Li-Ion cell. If that voltage is below 3.25V, it sends the ESP32 into a very long 'deep sleep' to protect the battery from draining too much. (Mental note to self: this ODROID-GO library routine should probably also turn off the TFT backlight and other power using components, otherwise there is not much battery protection at all!).

Now, measuring a voltage does not take very long. But if your measuring device is prone to fluctuations (like the one in the ESP32), you need to smoothen the measurements to get something reliable. The designer of that routine decided to use 64 individual measurements and aggregate them.

And this 64-times analog sampling-plus-processing loop took too long. The sound output problem was not in the ISR, but TZXLoop not filling the buffer fast enough.

For the correct function of TZXDuino it is essential that during playback the TZXLoop routine stays 'ahead' of the ISR routine. New period values (to make tones and pauses) must be generated fast enough for the buffer to never run 'empty'.

We cannot 100% make sure this happens.

But several measures make it more likely that all goes well during `play`:

1. Button checks are done only once every 50ms, and only the 'stop' and 'play' button are checked.
1. Text scrolling for long filenames is turned off.
1. LCD (or OLED) updates of tapeCounter and fileCounter alternate (only one of them will be called in a single TZXLoop run).

Finally, when the TZXLoop routine 'blocks' (wavePos and fillPos bit6 are the same), we check if the other bits in fillPos:

- if they are zero, all is well, TZXLoop (fillPos) is ahead of the ISR (wavePos) and has to wait until it finishes its current buffer part.
- if the other bits are non-zero, our buffer (fillPos) has been 'caught up from behind' by the ISR (wavePos).
  And then we print an error message (we are already in trouble, the extra time for the display does not matter anymore), so the user has at least a chance to spot this.
  We do not stop everything, or pause.

## ODROID-GO support

It all started in 2020, before the Covid-19 lockdown. A guy named Hagen loaded an FPGA-board with the design for the Jupiter Ace.

From its hardware, the Jupiter Ace is roughly comparable to a Sinclair ZX81. OK, it does not have BASIC but FORTH as the built-in language, that is a bit special. And it actually has video hardware to render a screen without too much help from the CPU.

So in the beginning of 2020 Hagen had a nicely synthesized Jupiter Ace hardware emulation running inside a 'Papilio One' FPGA, with a VGA output extension for a nice and crisp image on a VGA monitor, and a PS/2 keyboard interface. And then wrote his first small programs... and you realize that the machine only has tape in/out to save and load programs.

OK, there are tape emulators, most of them on Arduino. ArduiTape, CASDuino, TAPDuino, TZXDuino, to name a bunch. Write a tape archive file onto an SD card and an Arduino converts it into audio that a 'vintage' homecomputer can load. Like from a physical cassette tape, but more convenient, without fear of degradation (physical tape can wear out or lose its magnetism over time). No bulky cassette player, and hardly any moving parts required.

And you can buy these emulators - for example on Etsy (UK), a platform where small businesses offer hand-made, artistic and all kinds of niche products.

Arduitape 'Duncan Edwards' has an Arduino Nano with OLED screen, SD card reader, audio amplifier and a few buttons, all well put together in a sturdy case.
Let's get one! Problem: end of 2020, Brexit happened. Which made it more complicated to sell goods from the UK to Europe. So I could not get one.

Luckily, I _did_ happen to have an ODROID-GO. The ODROID-GO is a game console, made by the Hardkernel company from Korea (for the 10th anniversary of their Odroid device range), based on an ESP32 module from Espressif. And Espressif provides an Arduino core, and a development plug-in for the Arduino IDE. And Hardkernel provides an ODROID-GO library to make it easier to work with the hardware. The ODROID-GO has a ten-pin connector with I/O lines on the top. You can use it to control hardware from an Arduino Sketch.

There are several Tape Emulation projects active at the time (early 2021). The one I chose to work with was TZXDuino v1.17.

See, I am actually a software developer, for Embedded Systems, and for the full stack around them.

So I opened the TZXDuino v1.17 files and started learning.

To deeply understand code, I refactor it. At its core, refactoring preserves the function of a program, but re-structures it with a specific goal in mind. That goal can be to remove redundancies. Or make it better readable (e.g. renaming variables so that a name better fits a purpose).
The next step are 'obvious simplifications'. There are often several ways to do the same computation. But some of them might be easier or faster to compute on a smaller machine than others.

## Jupiter Ace tests

- 2021-03-26 - Testing with latest ODROID-GO version (simplified ISR)
  - TZX files: OK
  - TAP files: FAIL
- 2021-03-26 - Testing with first ODROID-GO version
  - TZX files: OK
  - TAP files: FAIL

TZX files work! Great!

But what about the TAP files?

First: check the specifications. TZXDuino does not explicitly name the Jupiter Ace anywhere, maybe some of the default Spectrum timings are a bit off?

The original [TZX specification v1.20](https://web.archive.org/web/20200418140438/https://www.worldofspectrum.org/TZXformat.html) also does not mention the Jupiter Ace.

The [Amstrad CDT specification from CPC Live](http://cpctech.cpc-live.com/docs/cdt.html) names a few more systems and hints at a potential problem:

----

### ZX Spectrum, Amstrad CPC, SAM Coupe, Jupiter ACE & Enterprise Tape Encoding

These computers share the same tape encoding, which is normal Frequency type encoding. Each bit is represented by one 'wave' of different duration for bit 0 and bit 1. Normally bit 1 is twice as big as bit 0. In the blocks, the timings are presented with Z80 T-states (cycles) per pulse. One wave is made from two pulses.

**IMPORTANT**

Z80 Clock rate is always timed at 3.5 MHz, so if the tape should be used on an Amstrad CPC with 4MHz clock then you should multiply ALL timings with 4/3.5 when you use that tape (this should be trivial), similary goes for SAM Coupe, that uses a 6MHz clock and Jupiter ACE that uses 3.2448 MHz and Enterprise that uses 4 MHz!

The Standard data blocks have always the same structure: First there is the Pilot Tone, then the two SYNC pulses (one wave) and after that the actual data (which can include a FLAG byte at the beginning and a Check-Sum byte at the end).

Here is a table showing the pulse timings for each part of the standard ROM load/save routine for each of these machines. If you have more accurate information then I would be glad to include it here. Enterprise has two loading speeds so timings for both are included. (more accurate timings and more info on Pilot length for the Enterprise will be provided in the next release of this format).

| Machine           | Pilot pulse | length |   Sync1 |    Sync2 |   Bit-0 |        Bit-1 |
| ----------------- | ----------: | -----: | ------: | -------: | ------: | -----------: |
| ZX Spectrum       |        2168 |    (1) |     667 |      735 |     855 |         1710 |
| SAM Coupe         |    58+19\*W |   6000 | 58+9\*H | 113+9\*H | 10+8\*W | 42+15\*W (4) |
| Amstrad CPC       |       Bit-1 |   4096 |   Bit-0 |    Bit-0 |     (2) |          (2) |
| Jupiter ACE       |        2011 |    (3) |     600 |      790 |     801 |         1591 |
| Enterprise (fast) |         742 |      ? |    1280 |     1280 |     882 |      602 (5) |
| Enterprise (slow) |        1750 |      ? |    2800 |     2800 |    1982 |     1400 (5) |

(1) The Spectrum uses different Pilot Lengths for Header and Data blocks. Header blocks have 8064 and data blocks have 3220 pilot pulses.

(2) Amstrad CPC ROM Load/Save routine can use variable speed for loading, so the Bit-1 pulse must be read from the Pilot Tone and Bit-0 can be read from the Sync pulses, and is always half the size of Bit-1. The Check-Sum is also different than the other two machines. The speed can vary from 1000 to 2000 baud.

(3) Jupiter ACE also uses different Pilot Lengths for Header and Data blocks. Header blocks have 8192 and Data blocks have 1024 pilot pulses. The last byte of the data is a XOR of all previous bytes (as in Spectrum). Also, at the end of everything there is a pulse of 762 T-states. The Header block has Sync byte (first byte after Sync pulses) 0 and the Data blocks can have anything but 0.

(4) The SAM Coupe timings can be user selected by a System Variable... the standard value being at 112, which is VERY close to ZX Spectrum loading speed, and therefore the 'Standard Speed Data' block can be used for those blocks. However if this System Variable is changed then the timings will change accordingly... in the above table the value of this variable is given as 'W', the 'H' value is 'W/2'. Of course the best way to determine it is to calculate it from the timings you get when sampling the Pilot tone... The values in the table could be by a fraction off the real ones, but it should not matter. Note: All timings are written in 3.5MHz clock. Also, there might be some junk bits (usually 7 or 8) AFTER the CheckSum (XOR) byte at the end of the block... they can just be ignored...

(5) The Enterprise stores data in a different way than all other computers do. It stores the Least Significant Bit first, but the Data blocks require the data to be Most Significant Bit first. This might lead to some confusion, but if the same mechanism is used to replay data for all machines then there will be no problem. Just store the data as Most Significant Bit first, but if you want to view the raw data in the TZX file then you will have to mirror each byte to get the correct values.

NOTE: The ZXTape format has ALL timings written according to a 3.5MHz clock... when you want to use it on a, for example, 4MHz clock based computer you should multiply ALL timings with 1.142857 (Do NOT use 4MHz or 6MHz base clock to store the timings in a TZX file!)

---

So the timings are somewhat different. Back to the code:

Hm. In TZXProcess, both TZX `ID10` and `TAP` just write a 'StandardBlock'. Except for the pause after the header/data block (which TZX stores, and TAP does not) there is no difference in processing. TZX files load, TAP does not, but the code that sends the data is not different.

Let's check the data written as TAP and TZX by the EightyOne emulator for the same - very short - test file:

```
Example file tape.tap

Offset

0000  1A 00                        Length of header (word, 26 bytes)
0002        00                     File type, Dictionary of Bytes (DICT == 0, BYTES != 0)
0003           74 61 70 65 20      "tape      " (filename, padded
0008  20 20 20 20 20               with spaces to 10 chars)
000D                 21 00         Length of file (word, 33 bytes)
000F                       51      Start address (word, 0x3C51 = 15441 for DICT)
0010  3C                           All following values are unused(*) for BYTES file:
0011     5C 3C                     DICT value of Current word
0013           4C 3C               DICT value of CURRENT system variable (0x3C31 = 15409)
0015                 4C 3C         DICT value of CONTEXT system variable (0x3C33 = 15411)
0017                       4F      DICT value of VOCLNK  system variable (0x3C35 = 15411)
0018  3C       
0019     72 3C                     DICT value of STKBOT  system variable (0x3C37 = 15411)
001B           11                  Checksum for header block

(*) Unused values are filled with spaces

File Data

001C              22 00           Length of data(word, 34 bytes)
001E                    4D 45     File content (dictionary definition)
0020  53 53 41 47 C5 1A 00 49
0028  3C 07 C3 0E 96 13 0D 00
0030  54 61 70 20 66 69 6C 65
0038  20 74 65 73 74 B6 04
003F                       01     Checksum for data block
```

Example file tape.tzx

```
File Header

Offset

0000  5A 58 54 61 70 65 21 1A       "ZXTape!"+char(26)
0008  01 0D                         TZX version 1.13

000A        30                      ID30: Text Description
000B           19                   ASCII text length = 25 bytes
000C              43 72 65 61       "Created with EightyOneTZX"
0010  74 65 64 20 77 69 74 68
0018  20 45 69 67 68 74 79 4F
0020  6E 65 54 5A 58

0025                 10             ID10: Standard speed block
0026                    B8 0B       3000 ms pause after block
0028  1B 00                         Length (including 00 flag byte)
002A        00                      HEADER flag byte 00
002B           00 74 61 70 65       "tape      " filename
0030  20 20 20 20 20 20 
0036                    21 00       Length of file (word, 33 bytes)
0038  51 3C 5C 3C 4C 3C 4C 3C       DICT system variables, see TAP file
0040  4F 3C 72 3C
0044              11                Checksum for header block

0045                 10             ID10: Standard speed block
0046                    B8 0B       3000 ms pause after block
0048  23 00                         Length (including FF flag byte)
004A        FF                      DATA flag byte FF
004B           4D 45 53 53 41       File content (dictionary definition)
0050  47 C5 1A 00 49 3C 07 C3
0058  0E 96 13 0D 00 54 61 70
0060  20 66 69 6C 65 20 74 65
0068  73 74 B6 04
006C              01                Checksum for data block
```

There is a one-byte difference between the data blocks:

Here is the start of the header block:

- TAP  0000  1A 00 00 74 61 70 65 20 20 20 20 20 ...
- TZX  0028  1B 00 `00` 00 74 61 70 65 20 20 20 20 ...

And here is the start of the data block:

- TAP  001C  22 00 4D 45 53 53 41 47 C5 1A 00 49 ...
- TZX  0048  23 00 `FF` 4D 45 53 53 41 47 C5 1A 00 ...

The [Jupiter Ace tape format](http://www.jupiter-ace.co.uk/doc_AceTapeFormat.html) does specify that the Jupiter Ace expects a 'block type' byte (not to be mixed up with the 'file type' byte) of `00` for header and `FF` for the data blocks.

> Looks like in my Jupiter Ace 'TAP' files, these '00' and 'FF' marker bytes are missing!

I tested this theory on a 'problematic' TAP file (tetris.tap), adding the missing bytes and correcting the block lengths with a HEX editor, and - success! - the Jupiter Ace then loaded this file!

(Hm. Maybe I can add an option to the TZXDuino code for adding the missing bytes.?)


## Refactoring

### SD FAT File system navigation

My part-refactored Arduino Uno version uses [SdFat from the library tree of the TZXDuino project by sadken](https://github.com/sadken/TZXDuino/tree/master/Libraries/SdFat).
Compiling for Arduino Uno resulted in:

> Sketch uses 27164 bytes (84%) of program storage space. Maximum is 32256 bytes.  
> Global variables use 1535 bytes (74%) of dynamic memory, leaving 513 bytes for local variables. Maximum is 2048 bytes.

The version with the new Storage.ino variant (cleaned up, up to 10 levels deep directory walk) uses these resources:

> Sketch uses 27328 bytes (84%) of program storage space. Maximum is 32256 bytes.  
> Global variables use 1490 bytes (72%) of dynamic memory, leaving 558 bytes for local variables. Maximum is 2048 bytes.

So this variant uses 164 bytes more program memory, but saves 45 bytes of RAM.

With [SdFat version 2](https://github.com/greiman/SdFat) we still can't change into the '..' directory.

If our code could use this, it would be much cleaner and would also have no limit on the depth of directories.

Compile for Arduino Uno with SdFat 2.0.5 and (not working) code that does this results in:

> Sketch uses 27780 bytes (86%) of program storage space. Maximum is 32256 bytes.  
> Global variables use 1476 bytes (72%) of dynamic memory, leaving 572 bytes for local variables. Maximum is 2048 bytes.

This is 616 bytes more program space, but RAM saving of 59 bytes.

As it is today, with SdFat 2.0.5 we get this result:
> Sketch uses 28036 bytes (86%) of program storage space. Maximum is 32256 bytes.  
> Global variables use 1498 bytes (73%) of dynamic memory, leaving 550 bytes for local variables. Maximum is 2048 bytes.

This is 872 bytes more program space, and 37 bytes RAM saved.

The SdFat 2.x library can be configured to include more or less functionality. There are some savings (around 500 bytes) but it's not really worthwhile to e.g. switch off ENABLE_DEDICATED_SPI or USE_FAT_FILE_FLAG_CONTIGUOUS, considering the performance loss.

Finally, I decided to implement openParent. It took two attempts to find a good way. My first version modified the `open` function to not suppress ".." files (ab-using O_SYNC), but that cluttered up the code. Eventually I found a much cleaner implementation that only needs added code in FatFile.h and FatFile.cpp.

[SdFat 2.0.6 with openParent()](https://github.com/hagen-git/SdFat) (pending pull request).
> Sketch uses 27810 bytes (86%) of program storage space. Maximum is 32256 bytes.
> Global variables use 1476 bytes (72%) of dynamic memory, leaving 572 bytes for local variables. Maximum is 2048 bytes.

Compared to the initial version this is 646 bytes more program space and 59 bytes less RAM.  

With the openParent() function in the SdFat 2.0.6 fork, these are the differences without and with using openParent(), based on an optimized implementation using a word array for an index walk:

NOT defined(HAS_OPEN_PARENT)
> Sketch uses 28036 bytes (86%) of program storage space. Maximum is 32256 bytes.  
> Global variables use 1498 bytes (73%) of dynamic memory, leaving 550 bytes for local variables. Maximum is 2048 bytes.

HAS_OPEN_PARENT
> Sketch uses 27810 bytes (86%) of program storage space. Maximum is 32256 bytes.  
> Global variables use 1476 bytes (72%) of dynamic memory, leaving 572 bytes for local variables. Maximum is 2048 bytes.

Delta: 226 program bytes saved, 22 RAM bytes saved with openParent().

----

We can't use the Arduino Uno 'SD.h' library, because it only supports short filenames. (The ESP32 variant does not have that limitation.) A workaround for this is using the 'SdFat.h' library.

Unfortunately, SdFat up to 2.0.6 is a bit 'braindead':

- There is no public openParent() method to go one directory up in the tree.
- That alone would not be too bad.
- But all 'open' methods actively skip over the FAT files '.' and '..'.
- This makes impossible to just use '..' to navigate one level up.

We can 'kludge' our way around this, store some directory information and drill our way down from root.

- One method (original code) for this is to store an array of directory names. This needs a lot of RAM or is dangerous - if the directory names are too long, they might overwrite other data. Also the original code was limited to three subdirectory levels (3 x 25 characters).
- Another variant would be to store a long filepath with all directories (I do this in the first ODROID-GO implementation).
- For the Arduino I now use a 'word' array to store the directory index number for the directories, up to ten levels deep (you can set this deeper). This uses a lot less RAM than the original directory name array - 20 bytes instead of 75, and it is much safer, because there is no danger of a very long directory name overwriting other data.

Next steps:

- Check if the same code also works on the ODROID-GO (with Espressif libraries). It would be nice to use the same code for all platforms.

----

### The TickToUs routine

The TZXDuino has a function that converts 'ticks' (based on a 3.5MHz CPU clock) into microseconds.

Here is the complete function from the original TZXDuino v1.17 code:

```C
word TickToUs(word ticks) {
  return (word) ((((float) ticks)/3.5)+0.5);
}
```

A very straightforward and accurate implementation to convert a number of ticks into the equivalent number of microseconds.
Use the input 'word' value (0..65535 on an 8-bit platform), divide by 3.5, round the result, and return as word.

There is a small problem here: the routine uses floating point arithmetic. And 8-bit CPUs are relatively bad at this.

Hm. A division by 3.5 is the same as 'multiply by 2 and divide by 7'. Like so:

```C
word TickToUs(word ticks) {
  return (word)((ticks * 2) / 7);
}
```

Nice. A multiplication by any power of two is the same as a 'left shift'. Like this:

```C
word TickToUs(word ticks) {
  return (word)((ticks << 1) / 7);
}
```

OK, but we are not functionally identical to the original. There are two problems.

1. We multiply the input by two, what if the input is larger than 32768? Then we lose information, because a 'word' can only hold 16 bits.
2. We don't round, but truncate the value (chop off everything after the decimal point).

Let's tackle the first problem first:

```C
word TickToUs(word ticks) {
  return (word)((long(ticks) << 1) / 7);
}
```

As an intermediate stage, we use 'long' - usually a four-byte datatype - to make sure our multiplication with two does not cause any 'overflow'.
Longer datatypes need more processing power and memory than sticking to byte or word datatypes, but still much less than floating point arithmetic.

But how about the rounding? We cannot add 1/2, that does not fit into integer datatypes. But we can do an approximation:

```C
word TickToUs(word ticks) {
  return (word)((long(ticks) << 1 + 3) / 7);
}
```

If we want to be accurate, we need 3.5, but we can't have that (integer numbers), right?

How about this?:

```C
word TickToUs(word ticks) {
  return (word)(((long(ticks) << 2) + 7) / 14);
}
```

This now computes the same result as the original floating point version, but only uses integer datatypes.

If we know that all input values are less than 8191 (up to 8190 == ((32767 - 7) >> 2)), a word is enough.

There is one more possible optimization - right-shifting a binary value divides by two:

```C
word TickToUs(word ticks) {
  return (word)((((long(ticks) << 2) + 7) >> 1) / 7);
}
```

But it is doubtful that this actually saves time - the integer-division routine probably already makes that optimization.

If we had to write our own calculation routines (integer division) and/or if space is really at a premium, we could also use other datatypes, e.g. three bytes instead of four-bytes.

Why is this interesting?

It helps to save time, and it saves program space.

- Any function that is 'built-in' into the compiler (like floating-point math) is normally implemented as a library.
- If you only need a single function of a library, often you still have to impport parts that you don't need.
- If your program only uses floating-point calculations at a single place, it might be worthwhile to see if you can replace it with integer math.
- If you can, you save the code space of the floating-point library (at tleast for parts of it).


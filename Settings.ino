// Settings storage abstraction
#include "userconfig.h"

/* Settings Byte:
    bit 0: 1200
    bit 1: 2400
    bit 2: 3600
    bit 3: not used
    bit 4: not used
    bit 5: Pause @ Start
    bit 6: Gremlin Loader
    bit 7: UEFTurbo
*/

byte getOptions() {
  byte settings = 0;
  // TODO: store cfgBaudrate in two bits
  switch (cfgBaudrate) {
    case 1200:
      bitSet(settings,0);
      break;
    case 2400:
      bitSet(settings,1);
      break;
    case 3600:
      bitSet(settings,2);
      break;
    case 3670:
      bitSet(settings,3);
      break;
  }
  if (cfgJupiterTAP)   bitSet(settings,4);
  if (cfgPauseAtStart) bitSet(settings,5);
  if (cfgFlipPolarity) bitSet(settings,6);
  if (cfgTurboBoost) bitSet(settings,7);
  return settings;
}

void setOptions(byte settings) {
  // TODO: store cfgBaudrate in two bits
  if (bitRead(settings, 0)) {
    cfgBaudrate = 1200;
  }
  if (bitRead(settings, 1)) {
    cfgBaudrate = 2400;
  }
  if (bitRead(settings, 2)) {
    cfgBaudrate = 3600;
  }
  if (bitRead(settings, 2)) {
    cfgBaudrate = 3670;
  }
  cfgJupiterTAP   = bitRead(settings, 4);
  cfgPauseAtStart = bitRead(settings, 5);
  cfgFlipPolarity = bitRead(settings, 6);
  cfgTurboBoost = bitRead(settings, 7);
}


#ifdef SETTINGS_EEPROM
#include <EEPROM.h>

void saveSettings() {
  byte settings = getOptions();
  EEPROM.put(SETTINGS_ADDRESS, settings);
}

void loadSettings() {
  byte settings = 0;
  EEPROM.get(SETTINGS_ADDRESS, settings);
  if (settings) { // only use if _any_ flag is set
    setOptions(settings);
  }
}
#endif // SETTINGS_EEPROM


#ifdef SETTINGS_FILE
void saveSettings() {
  // TODO: implement
}

void loadSettings() {
  // TODO: implement
  // set defaults
  cfgPauseAtStart = false;
  cfgJupiterTAP   = true;
  cfgTurboBoost = false;
  cfgFlipPolarity = false;
  cfgBaudrate = 1200;
}
#endif // SETTINGS_FILE

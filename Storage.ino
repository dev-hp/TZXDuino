// Storage abstraction
#include "userconfig.h"
#include "Storage.h"


#ifdef PLATFORM_ODROID_GO

// uses SD library
File curDir;
File curEntry;

#define MAXLEN_CURPATH 256
char curPath[MAXLEN_CURPATH + 1];  // full directory pathName
int currentFile = 0;               //Current position in directory

void Storage_setup(void) {
  pinMode(STORAGE_SD_CS_PIN, OUTPUT);     //Setup SD card chipselect pin
  if (!SD.begin(STORAGE_SD_CS_PIN)) {
    printtextF(PSTR("No SD Card"), 0);
    return;
  }
  curDir = SD.open("/");
  currentFile = 0;
  seekFile(1);
}

int getLastSlashPos(const char *s) {
  // find last '/' in pathname
  int pos = 0;
  int ldp = 0;
  while (*s) {
    if (*s == '/') {
      ldp = pos;
    }
    pos++;
    s++;
  }
  return ldp;
}

void seekFile(int pos) {
  //move to a set position in the directory
  if (pos < 1) {
    // position 0 => go to last file in directory
    curEntry.close(); // in case the directory is empty
    // go to last file in directory
    File nextEntry;
    while (nextEntry = curDir.openNextFile()) {
      curEntry.close();
      curEntry = nextEntry;
      currentFile++;
    }
  } else if ((pos != currentFile) || !curEntry) {
    // we only seek if we don't have this file already open
    curEntry.close();
    if (pos <= currentFile) {
      // position is before current position - rewind
      curDir.rewindDirectory();
      currentFile = 0;
    }
    while (currentFile < pos) {
      curEntry.close();
      curEntry = curDir.openNextFile();
      currentFile++;
    }
    if (!curEntry) {
      // no file at all, or after the last entry. Try to open the first file
      curDir.rewindDirectory();
      curEntry = curDir.openNextFile();
      currentFile = 1;
    }
  } // (else) we just need to read file information
  if (curEntry) {
    filesize = curEntry.size();
    isDir = curEntry.isDirectory();
    // store the filePath
    strncpy(curPath, curEntry.name(), MAXLEN_CURPATH);
    curPath[MAXLEN_CURPATH] = '\0'; // paranoia
    // copy the filename part
    strncpy(filename, &curPath[getLastSlashPos(curPath) + 1], MAXLEN_LONGFILENAME);
    filename[MAXLEN_LONGFILENAME] = '\0'; // paranoia
  } else {
    // empty directory, no file
    currentFile = 1;
    filesize = 0;
    isDir = false;
    // leave the old path in curPath
    // but set a filename to inform the user
    strcpy_P(filename, PSTR("<no_file_found>"));
  }
  // show on screen
  msgSelectFile();
  setScrollText(filename);
}

void upFile() {
  //move up a file in the directory
  seekFile(currentFile - 1);
}

void downFile() {
  //move down a file in the directory
  seekFile(currentFile + 1);
}

void changeDir() {
  curEntry.close();
  curDir.close();
  curDir = SD.open(curPath);
  currentFile = 0;
  seekFile(1); // first file in directory
}

void returnFromSubdir() {
  // get the current directory as curPath
  // e.g. we could have done this: strncpy(curPath, curDir.name(), MAXLEN_CURPATH);
  // smallest position of getLastSlashPos is 0, so we stay at "/"
  int pos = getLastSlashPos(curPath);
  curPath[pos] = '\0';
  pos = getLastSlashPos(curPath);
  if (!pos) {
    // root dir is special, because we need to keep its 'slash'
    curPath[pos++] = '/'; // paranoia, should already be there
  }
  curPath[pos] = '\0';
  changeDir();
}

bool seekPosition(unsigned long p) {
  return curEntry.seek(p);
}

int readBuf(byte *buf, byte len) {
  // we could read size_t lengths, but the code does not use this, so a byte is enough
  return curEntry.read(buf, len);
}

bool curFileExists(void) {
  if (!!curEntry) {
    return true; // already open, file exists
  }
  seekFile(currentFile); // file might have been closed, seek it
  return !!curEntry; // if the entry is not a nullptr, a file exists
}

bool curFileReadOpen(void) {
  return !!curEntry; // the file is already open
}

void curFileClose(void) {
  if (!!curEntry) {
    curEntry.close();
  }
}
#endif // PLATFORM_ODROID_GO


#ifdef PLATFORM_ARDUINO

// uses SdFat library
static SdFat sd;
static FatFile curDir;
static FatFile curEntry;

#define HAS_OPEN_PARENT 1
#ifndef HAS_OPEN_PARENT
// SdFat (up to and including 2.0.6) actively hides any functionality to reach the parent directory.
// Therefore we must kludge our way around this limitation.
#define DIRLEVEL_ROOT (-1)
#define DIRLEVEL_MAX  10
static word allDirLevel[DIRLEVEL_MAX];
static int curDirLevel = DIRLEVEL_ROOT;
#endif // not HAS_OPEN_PARENT

static int curFileIdx = 0;  //Current position in directory

void Storage_setup(void) {
  pinMode(STORAGE_SD_CS_PIN, OUTPUT);     //Setup SD card chipselect pin
  if (!sd.begin(STORAGE_SD_CS_PIN, SPI_FULL_SPEED)) {
    printtextF(PSTR("No SD Card"), 0);
    return;
  }
  curDir = sd.open("/", O_READ);
  curFileIdx = 0; // dir index goes from 1 to n
  seekFile(1);
}

void seekFile(int pos) {
  //move to a set position in the directory
  if (pos < 1) {
    // position 0 => go to last file in directory
    curEntry.close(); // in case the directory is empty
    // go to last file in directory
    FatFile nextEntry;
    while (nextEntry.openNext(&curDir, O_READ)) {
      curEntry.close();
      curEntry = nextEntry;
      curFileIdx++;
      nextEntry.close();
    }
  } else if ((pos != curFileIdx) || !curEntry.isOpen()) {
    // we only seek if we don't have this file already open
    curEntry.close();
    if (pos <= curFileIdx) {
      // position is before current position - rewind
      curDir.rewind();
      curFileIdx = 0;
    }
    while (curFileIdx < pos) {
      curEntry.close();
      curEntry.openNext(&curDir, O_READ);
      curFileIdx++;
    }
    if (!curEntry.isOpen()) {
      // no file at all, or after the last entry. Try to open the first file
      curDir.rewind();
      curEntry.openNext(&curDir, O_READ);
      curFileIdx = 1;
    }
  } // (else) we just need to read file information
  if (curEntry.isOpen()) {
    filesize = curEntry.fileSize();
    isDir = curEntry.isDir();
    curEntry.getName(filename, MAXLEN_LONGFILENAME);
  } else {
    // empty directory, no file
    curFileIdx = 1;
    filesize = 0;
    isDir = false;
    strcpy_P(filename, PSTR("<no_file_found>"));
  }
  // show on screen
  msgSelectFile();
  setScrollText(filename);
}

void upFile() {
  //move up a file in the directory
  seekFile(curFileIdx - 1);
}

void downFile() {
  //move down a file in the directory
  seekFile(curFileIdx + 1);
}

void changeDir() {
#ifdef HAS_OPEN_PARENT
  if (curEntry.isDir()) {
    curDir.close();
    curDir = curEntry;
    curEntry.close();
    curFileIdx = 0;
    seekFile(1); // first file in directory
  }
#else // not HAS_OPEN_PARENT
  if (curEntry.isDir() && curDirLevel < DIRLEVEL_MAX) {
    if (curDirLevel > DIRLEVEL_ROOT) {
      allDirLevel[curDirLevel] = curDir.dirIndex();
    }
    curDirLevel++;
    curDir.close();
    curDir = curEntry;
    curEntry.close();
    curFileIdx = 0;
    seekFile(1); // first file in directory
  }
#endif // (not) HAS_OPEN_PARENT
}

void returnFromSubdir() {
#ifdef HAS_OPEN_PARENT
  curEntry.close();
  if (curEntry.openParent(&curDir)) {
    curDir.close();
    curDir = curEntry;
    curEntry.close();
  }
  curFileIdx = 0;
  seekFile(1); // first file in directory
#else // not HAS_OPEN_PARENT
  // We need to kludge our way to the parent directory. Ouch.
  // Start at root
  curDir.close();
  curDir = sd.open("/", O_READ);
  // let's do a directory walk - via INDEX
  curEntry.close();
  for (int i = 0; i < curDirLevel; i++) {
    if (curEntry.open(&curDir, allDirLevel[i], O_READ)) {
      curDir.close();
      curDir = curEntry;
      curEntry.close();
    }
  }
  curDirLevel--;
  curFileIdx = 0;
  seekFile(1); // first file in directory
#endif // (not) HAS_OPEN_PARENT
}

bool seekPosition(unsigned long p) {
  return curEntry.seekSet(p);
}

int readBuf(byte *buf, byte len) {
  // we could read size_t lengths, but the code does not use this, so a byte is enough
  return curEntry.read(buf, len);
}

bool curFileExists(void) {
  if (curEntry.isOpen()) {
    return true; // already open, file exists
  }
  seekFile(curFileIdx); // file might have been closed, seek it
  return curEntry.isOpen();
}

bool curFileReadOpen(void) {
  return curEntry.isOpen(); // should be true
}

void curFileClose(void) {
  if (curEntry.isOpen()) {
    curEntry.close();
  }
}
#endif // PLATFORM_ARDUINO

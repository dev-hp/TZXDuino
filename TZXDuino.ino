/*
                                      TZXduino
                               Written and tested by
                            Andrew Beer, Duncan Edwards
                            www.facebook.com/Arduitape/

                Designed for TZX files for Spectrum (and more later)
                Load TZX files onto an SD card, and play them directly
                without converting to WAV first!

                Directory system allows multiple layers,  to return to root
                directory ensure a file titles ROOT (no extension) or by
                pressing the Menu Select Button.

                Written using info from worldofspectrum.org
                and TZX2WAV code by Francisco Javier Crespo

 *              ***************************************************************
                Menu System:
                  TODO: add ORIC and ATARI tap support, clean up code, sleep

                V1.0
                Motor Control Added.
                High compatibility with Spectrum TZX, and Tap files
                and CPC CDT and TZX files.

                V1.32 Added direct loading support of AY files using the SpecAY loader
                to play Z80 coded files for the AY chip on any 128K or 48K with AY
                expansion without the need to convert AY to TAP using FILE2TAP.EXE.
                Download the AY loader from http://www.specay.co.uk/download
                and load the LOADER.TAP AY file loader on your spectrum first then
                simply select any AY file and just hit play to load it. A complete
                set of extracted and DEMO AY files can be downloaded from
                http://www.worldofspectrum.org/projectay/index.htm
                Happy listening!

                V1.8.1 TSX support for MSX added by Natalia Pujol

                V1.8.2 Percentage counter and timer added by Rafael Molina Chesserot along with a reworking of the OLED1306 library.
                Many memory usage improvements as well as a menu for TSX Baud Rates and a refined directory controls.

                V1.8.3 PCD8544 library changed to use less memory. Bitmaps added and Menu system reduced to a more basic level.
                Bug fixes of the Percentage counter and timer when using motor control/

                V1.11 Added unzipped UEF playback and turbo UEF to the Menu thatks to the kind work by kernal@kernalcrash.com
                Supports Gunzipped UEFs only.

                v1.13   HQ.UEF support added by Rafael Molina Chesserot of Team MAXDuino
                v1.13.1 Removed digitalWrite in favour of a macro suggested by Ken Forster
                Some MSX games will now work at higher Baudrates than before.
                v1.13.2 Added a fix to some Gremlin Loaders which reverses the polarity of the block.
                New menu Item added. "Gremlin Loader" turned on will allow Samurai Trilogy and Footballer of the Year II
                CDTs to load properly.

                1.14 ID15 code adapted from MAXDuino by Rafael Molina Chasserot.
                Not working 100% with CPC Music Loaders but will work with other ID15 files.

                1.14.2 Added an ID15 switch to Menu as ID15 being enabled was stopping some files loading properly.
                1.14.3 Removed the switch in favour of an automatic system of switching ID15 routine on and off.

                1.15 Added support for the Surenoo RGB Backlight LCD using an adapted version of the Grove RGBLCD library.
                Second counter not currently working. Also some memory saving tweaks.

                1.15.3 Adapted the MAXDuino ID19 code and TurboMode for ZX80/81
                Also added UEF Chunk 117 which allows for differing baudrates in BBC UEFs.
                Added a Spectrum Font for OLED 1306 users converted by Brendan Alford
                Added File scrolling by holding up or down buttons. By Brendan Alford.

                1.16 Fixed a bug that was stopping Head Over Heels (and probably others)loading on +2 Spectrum. Seems to have made
                ZX80/81 playback alot more stable too.

                1.17 Added ORIC TAP file playback from the Maxduino team.
*/

/*
   Required external libraries (tested version in brackets):
   - SdFat (2.0.5) by Bill Greiman - the Arduino built-in SD library does not support long filenames
   - TimerOne (1.1.0) - interrupt-driven exact timing
   If you use an LCD module (e.g. 16 characters and 2 lines):
   - LiquidCrystal_I2C by FRank de Brabander (1.1.2) - used for 1602 LCD with an I2C adapter

   If you use an 'ODROID-GO':
   - ODROID-GO Arduino library (1.1.x) by hardkernel/Lee Seungcheol from https://github.com/hardkernel/ODROID-GO
   - Arduino ESP32 (1.0.5) (follow the instructions on https://wiki.odroid.com/, linkd by the ODROID-GO library)
   - TimerOne (1.x.x) with support for ESP32 from https://github.com/hagen-git/TimerOne
*/

#define VERSION "TZXDuino 1.17"

#include "userconfig.h"

#include "Buttons.h"
#include "Display.h"
#include "Storage.h"
#include "TZXDuino.h"

void menuSettings();  // from menu.ino

bool bMotorSense = false;     //Current sensed motor control state
bool bOldMotorSense = false;  //Last motor control state

bool bPlaying = false;  //Currently playing flag
bool bPaused = false;   //Pause state

bool isDir = false;     //Is the current file a directory?
int browseDelay = 500;  //Delay between up/down navigation


#define POLL_INTERVAL_MS 50  //20Hz is fast enough
unsigned long nextPoll = 0;  //time for next button poll

void setup() {
  Display_setup();
  msgStarting();
  Buttons_setup();
  Storage_setup();
  TZXSetup();  //Setup TZX specific options
  loadSettings();
#ifdef PLATFORM_ODROID_GO
  GO.Speaker.mute();  // mute the speaker
  GO.battery.setProtection(true);  // shutdown battery if voltage is too low
#endif
  nextPoll = millis();
}

void loop() {
  if (bPlaying) {
    TZXLoop();  //Only run if a file is playing, keep the buffer full
  } else {
    AUDIO_OUT(LOW);  //Keep output LOW while no file is playing.
    //Scroll filename only if not playing, otherwise (I2C) display
    //writes can slow down the TZXLoop waveBuffer fill too much
    updateScrollText();
  }

  if (millis() > nextPoll) { // check buttons every 50ms
    nextPoll += POLL_INTERVAL_MS;

    if (isPlayPressed()) {
      //Handle Play/Pause button
      if (bPlaying) {
        //If a file is playing, pause or unpause the file
        bPaused = !bPaused;
        printCounters();
        msgPlaying(); // update Playing or Paused state
      } else {
        //If no file is playing, start playback
        playFile();
        delay(200);
      }
      while (isPlayPressed()) {
        //prevent button repeats by waiting until the button is released.
        delay(200);
      }
    }

    if (isStopPressed()) {
      if (bPlaying) {
        stopFile();
      } else {
        returnFromSubdir();
      }
      while (isStopPressed()) {
        //prevent button repeats by waiting until the button is released.
        delay(200);
      }
    }

    if (bPlaying) {
      // we only need to check the motor sense pin if we are playing
      bMotorSense = isMotorSenseActive();
      if (bOldMotorSense != bMotorSense) {
        //Motor control normally works by pulling the pinMotor pin to ground to play, and NC to stop
        //if pause is active and motor sense becomes active, we need to un-pause
        //if pause in not on, and motor sense turns off, we must pause
        if (bMotorSense == bPaused) { //update if both set or both reset
          bPaused = !bPaused;
          msgPlaying();
          printCounters();
        }
        bOldMotorSense = bMotorSense;
      }
    } else { // not bPlaying
#ifdef PLATFORM_ODROID_GO
      // GO.update(); // will internally call GO.battery.update(), we don't need the other stuff
      GO.battery.update();  // monitor and protect the battery from draining too low (3.25V)
      // takes 64 analog samples of the Battery voltage, starving TZXLoop if not guarded
      // TODO: replace ODROID-GO battery routine with code that also turns off display and LED
#endif
      // we only look at 'menu', 'up' and 'down' buttons if not playing
      // 'menu' is checked first
      if (isRootPressed()) {
        menuSettings();
        printtextF(PSTR(VERSION), 0);
        setScrollText(filename);
        while (isRootPressed()) {
          //prevent button repeats by waiting until the button is released.
          delay(200);
        }
      }
      // 'up' and 'down' are mutually exclusive.
      // ('down' wins if you have hardware that supports both simultaneously)
      if (isDownPressed()) {
        //Move down a file in the directory
        resetScrollPos();
        downFile();
        delay(browseDelay);
        reduceBrowseDelay();
      } else if (isUpPressed()) {
        //Move up a file in the directory
        resetScrollPos();
        upFile();
        delay(browseDelay);
        reduceBrowseDelay();
      } else {
        resetBrowseDelay();
      }
      // no debounce for 'up' and 'down', because we _want_ them to repeat
    } // (not) bPlaying

  }  // check buttons every 50ms

}  // Loop

void reduceBrowseDelay() {
  if (browseDelay >= 100) {
    browseDelay -= 100;
  }
}

void resetBrowseDelay() {
  browseDelay = 500;
}

void stopFile() {
  TZXStop();
  if (bPlaying) {
    msgStopped();
    bPlaying = false;
  }
}

void playFile() {
  if (isDir) { //If selected file is a directory, enter it
    changeDir();
  } else { // not isDir
    if (curFileExists()) {
      bPaused = cfgPauseAtStart;
      msgPlaying();
      setScrollText(filename);
      TZXPlay(filename);  //Load file and play
      bPlaying = true;
      if (bPaused) {
        TZXPause();
      }
    } else {// not curFileExists
      printtextF(PSTR("No File Selected"), 1);
    }// (not) curFileExists
  }// (not) isDir
}

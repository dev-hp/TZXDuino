// UEF Processing
#include "userconfig.h"
#include "TZXDuino.h"

static word chunkID = 0;
static byte UEFPASS = 0;
static byte passforZero = 2;
static byte passforOne = 4;

static byte wibble = 1;
static byte parity = 0 ;         // 0:NoParity 1:ParityOdd 2:ParityEven (default:0)

//Arduino Uno:
//- Program storage: 27680 bytes enabled, 27164 disabled
//- no additional global variables
//#define FLOATING_POINT_SUPPORT

void UEF_GetChunkID(void) {
  // set after GETUEFHEADER
  if (ReadWord(bytesRead)) {
    chunkID = outWord;
    if (ReadDword(bytesRead)) {
      bytesToRead = outLong;
      parity = 0;
      if (chunkID == ID0104) {
        bytesToRead -= 3;
        bytesRead++;
        //grab 1 byte Parity
        if (ReadByte(bytesRead) == 1) {
          if (outByte == 'O')
            parity = wibble ? 2 : 1;
          else if (outByte == 'E')
            parity = wibble ? 1 : 2;
          else
            parity = 0;  // 'N'
        }
        bytesRead++;
      }
    } else {
      chunkID = IDCHUNKEOF;
    }
  } else {
    chunkID = IDCHUNKEOF;
  }
  if (cfgTurboBoost) {
    zeroPulse = UEFTURBOZEROPULSE;
    onePulse = UEFTURBOONEPULSE;
  } else {
    zeroPulse = UEFZEROPULSE;
    onePulse = UEFONEPULSE;
  }
  bLastByte = false;
  //reset data block values
  currentBit = 0;
  currentPass = 0;
  //set current task to PROCESSCHUNKID
  currentTask = PROCESSCHUNKID;
  currentBlockTask = READPARAM;
  UEFPASS = 0;
}

void UEF_ProcessChunkID(void) {
  //CHUNKID Processing
  switch (chunkID) {
    case ID0000:
      bytesRead += bytesToRead;
      currentTask = GETCHUNKID;
      break;
    case ID0100:
      writeUEFData();
      break;
    case ID0104:
      writeUEFData();
      break;
    case ID0110:
      if (currentBlockTask == READPARAM) {
        if (ReadWord(bytesRead)) {
          if (cfgTurboBoost) {
            pilotPulses = UEFTURBOPILOTPULSES;
            pilotLength = UEFTURBOPILOTLENGTH;
          } else {
            pilotPulses = UEFPILOTPULSES;
            pilotLength = UEFPILOTLENGTH;
          }
        }
        currentBlockTask = PILOT;
      } else {
        UEFCarrierToneBlock();
      }
      break;
    case ID0111:
      if (currentBlockTask == READPARAM) {
        if (ReadWord(bytesRead)) {
          pilotPulses = UEFPILOTPULSES;  // for TURBOBAUD1500 is outWord<<2
          pilotLength = UEFPILOTLENGTH;
        }
        currentBlockTask = PILOT;
        UEFPASS++;
      } else if (UEFPASS == 1) {
        UEFCarrierToneBlock();
        if (pilotPulses == 0) {
          currentTask = PROCESSCHUNKID;
          currentByte = 0xAA;
          bLastByte = true;
          currentBit = 10;
          currentPass = 0;
          UEFPASS = 2;
        }
      } else if (UEFPASS == 2) {
        parity = 0;  // NoParity
        writeUEFData();
        if (currentBit == 0) {
          currentTask = PROCESSCHUNKID;
          currentBlockTask = READPARAM;
        }
      } else if (UEFPASS == 3) {
        UEFCarrierToneBlock();
      }
      break;
    case ID0112:
      if (ReadWord(bytesRead)) {
        if (outWord > 0) {
          workPauseLength = outWord;
          currentID = IDPAUSE;
          currentPeriod = workPauseLength;
          bitSet(currentPeriod, 15);
        }
        currentTask = GETCHUNKID;
      }
      //}
      break;
    case ID0114:
      if (ReadWord(bytesRead)) {
        pilotPulses = UEFPILOTPULSES;
        bytesRead -= 2;
      }
      UEFCarrierToneBlock();
      bytesRead += bytesToRead;
      currentTask = GETCHUNKID;
      break;
    case ID0116:
      if (ReadDword(bytesRead)) {
        // (IEEE 754 floating-point representation for a wait time.)
#ifdef FLOATING_POINT_SUPPORT
        { // this is the only place where a float is used
          float outFloat = *((float *)&outLong) * 1000; // GAP time in seconds
          outWord = (word)outFloat; // chopped down to a word
        }
#else
        outWord = 42; // TODO: check if we can (partially) decode the floating point value with integer arithmetic
#endif
        if (outWord) {
          workPauseLength = outWord;
          currentID = IDPAUSE;
          currentPeriod = workPauseLength;
          bitSet(currentPeriod, 15);
        }
        currentTask = GETCHUNKID;
      }
      break;
    case ID0117:
      if (ReadWord(bytesRead)) {
        if (outWord == 300) {
          passforZero = 8;
          passforOne = 16;
        } else {
          passforZero = 2;
          passforOne = 4;
        }
        currentTask = GETCHUNKID;
      }
      break;
    case IDCHUNKEOF:
      bytesRead += bytesToRead;
      stopFile();
      return;
    default:
      bytesRead += bytesToRead;
      currentTask = GETCHUNKID;
      break;
  }
}


void ReadUEFHeader() {
  //Read and check first 12 bytes for a UEF header
  byte uefHeader[9];
  int i = 0;

  if (seekPosition(0)) {
    i = readBuf(uefHeader, 9);
    if (memcmp_P(uefHeader, UEFFile, 9) != 0) {
      printtextF(PSTR("Not UEF File"), 1);
      TZXStop();
    }
  } else {
    msgErrFileRead();
  }
  bytesRead = 12;
}

void UEFCarrierToneBlock() {
  //Pure Tone Block - Long string of pulses with the same length
  currentPeriod = pilotLength;
  pilotPulses--;
  if (pilotPulses == 0) {
    currentTask = GETCHUNKID;
  }
}

void writeUEFData() {
  //Convert byte from file into string of pulses.  One pulse per pass
  if (currentBit == 0) {  //Check for byte end/first byte
    if (ReadByte(bytesRead)) {  //Read in a byte
      currentByte = outByte;
      bytesToRead--;
      bitChecksum = 0;
      if (bytesToRead == 0) {  //Check for end of data block
        bLastByte = true;
        if (nextPauseLength == 0) {  //Search for next ID if there is no pause
          currentTask = PROCESSCHUNKID;
        } else {
          currentBlockTask = PAUSE;  //Otherwise start the pause
        }
        //return;                               // exit
      }
    } else {  // We reached the EOF
      currentTask = GETCHUNKID;
    }
    currentBit = 11;
    currentPass = 0;
  }

  if ((currentBit == 2) && (parity == 0)) currentBit = 1;  // parity N

  if (currentBit == 11) {
    currentPeriod = zeroPulse;
  } else if (currentBit == 2) {
    currentPeriod = (bitChecksum ^ (parity & 1)) ? onePulse : zeroPulse;
  } else if (currentBit == 1) {
    currentPeriod = onePulse;
  } else {
    //Set next period depending on value of bit 0
    currentPeriod = (currentByte & 1) ? onePulse : zeroPulse;
  }
  currentPass++;  //Data is played as 2 x pulses for a zero, and 4 pulses for a one when speed is 1200

  if (currentPeriod == zeroPulse) {
    if (currentPass == passforZero) {
      if ((currentBit > 1) && (currentBit < 11)) {
        currentByte >>= 1;  //Shift along to the next bit
      }
      currentBit--;
      currentPass = 0;
      if (bLastByte && (currentBit == 0)) {
        currentTask = GETCHUNKID;
      }
    }
  } else {
    // must be a one pulse
    if (currentPass == passforOne) {
      if ((currentBit > 1) && (currentBit < 11)) {
        bitChecksum ^= 1;
        currentByte >>= 1;  //Shift along to the next bit
      }
      currentBit--;
      currentPass = 0;
      if (bLastByte && (currentBit == 0)) {
        currentTask = GETCHUNKID;
      }
    }
  }
}

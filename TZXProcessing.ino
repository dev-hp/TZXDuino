// TZX Processing
#include "userconfig.h"
#include "TZXDuino.h"

//#define TZXDEBUG 1
//#define ZERO_PERIOD_DEBUG 1

//Main Variables
static byte inputBuf[11];
static byte procSpread;
static byte fillPos;
static byte blockChecksum;
static byte ayPass;
static word ayBlkLen;
static word directRecordingPeriod;
static bool bOutState;
static bool bPreFill;
static bool bID15WantLevelSet;
static byte ayHeaderPos;
static byte currentChar;


//Interrupt Service Routine (ISR) Defines
#define WBUFSIZE            0x80
#define WBUFMASK            0x7F
#define WBUFSLICE           0x40
#define WAV_BIT_PAUSE         15
#define WAV_BIT_LEVEL         14
#define WAV_MSK_PERIOD    0x3FFF

//Interrupt Service Routine (ISR) Variables
static volatile word waveBuffer[WBUFSIZE];
static volatile byte wavePos = 0;
static volatile bool bIsRunning = false;

//Interrupt Service Routine (ISR)
void waveISR(void) {
  unsigned long newPeriod = 100000UL; // check again in 100ms
  if (bIsRunning) {
    word workingPeriod = waveBuffer[wavePos];
    if (workingPeriod == 0) {
      newPeriod = 1000; // buffer empty, re-check in 1ms
      // do not advance wave position, do not toggle output
    } else {
      byte bitLevel = bitRead(workingPeriod, WAV_BIT_LEVEL);
      AUDIO_OUT(bitLevel);
      bool bitPause = bitRead(workingPeriod, WAV_BIT_PAUSE);
      newPeriod = workingPeriod & WAV_MSK_PERIOD;
      if (bitPause) { // pauses are in milliseconds, not microseconds
        newPeriod <<= 10;
        // we correct the multiplyer from 1000 to 1024 in IDPAUSE processing
      }
      wavePos++;
      wavePos &= WBUFMASK; // keep wavePos inside buffer, automagically flip buffer pages :-)
    }
  }
  Timer1.setPeriod(newPeriod);  // next pulse length
} // waveISR


void TZXSetup(void) {
#ifdef PLATFORM_ODROID_GO
  vPortCPUInitializeMutex(&waveMutex);
#else
#if defined(TZXDEBUG) || defined(ZERO_PERIOD_DEBUG)
  Serial.begin(115200);
  Serial.println("TZXDEBUG");
#endif // TZXDEBUG || ZERO_PERIOD_DEBUG
#endif
  pinMode(AUDIO_OUTPUT_PIN, OUTPUT);  //Set output pin
  AUDIO_OUT(LOW);                     //Start output LOW
  bIsRunning = false;
  // wavePos = 0; -- automatically done
  Timer1.initialize(100000UL);  //100ms pause prevents anything bad happening before we're ready
  Timer1.attachInterrupt(waveISR);
  Timer1.stop();  //Stop the timer until we're ready
}


static void clearBuffer(void) {
  for (int i = 0; i < WBUFSIZE; i++) {
    waveBuffer[i] = 0;
  }
}


static word TickToUs(word ticks) {
  // Our base frequency is 1MHz (periods are in microseconds).
  // The TZX 'tick' frequency is 3.5MHz (Z80 clock speed).
  // So any given 'ticks' duration must be divided by 3.5 to get to the number of microseconds.
  return (word)(((long(ticks) << 2) + 7) / 14); // with exact rounding - all with integer math
}


// This way we need to compute the filename length only once.
#define checkForTap (strstr_P(strlwr(filename + (len - 4)), PSTR(".tap")))
#define checkForP (strstr_P(strlwr(filename + (len - 2)), PSTR(".p")))
#define checkForO (strstr_P(strlwr(filename + (len - 2)), PSTR(".o")))
#define checkForAY (strstr_P(strlwr(filename + (len - 3)), PSTR(".ay")))
#define checkForUEF (strstr_P(strlwr(filename + (len - 4)), PSTR(".uef")))

static void checkForEXT(char *filename) {
  byte len = strlen(filename);
  if (checkForTap) { //Check for Tap File.  As these have no header we can skip straight to playing data
    currentTask = PROCESSID;
    currentID = TAP;
    if (readBuf(inputBuf, 1) == 1) {
      if (inputBuf[0] == 0x16) {
        currentID = ORIC;
      }
    }
  } else if (checkForP) {  //Check for P File.  As these have no header we can skip straight to playing data
    currentTask = PROCESSID;
    currentID = ZXP;
  } else if (checkForO) {  //Check for O File.  As these have no header we can skip straight to playing data
    currentTask = PROCESSID;
    currentID = ZXO;
  } else if (checkForAY) {  //Check for AY File.  As these have no TAP header we must create it and send AY DATA Block after
    currentTask = GETAYHEADER;
    currentID = AYO;
  } else if (checkForUEF) {  //Check for UEF File.  As these have no TAP header we must create it and send UEF DATA Block after
    currentTask = GETUEFHEADER;
    currentID = UEF;
  }
  // else we assume we have a TZX file, maybe with a different extension (like .cdt for Amstrad)
}

void TZXPlay(char *filename) {
  Timer1.stop();                 //Stop timer interrupt
  // ISR data reset
  bIsRunning = false;
  clearBuffer();
  wavePos = WBUFMASK; // get the wave buffer position out of the way, so we can fill the lower part of the buffer
  fillPos = 0;
  bPreFill = true;
  // Counter reset
  tapeCounterReset();            //Reset the 3-digit 'tape counter'
  filePercent = 0;               //reset percent counter
  filePercentPrint();            //and print it
  bytesRead = 0;                 //start of file
  // Open file, only continue if that went well
  if (!curFileReadOpen()) {      //open file and check for errors
    msgErrFileRead();
    return;
  }
  currentTask = GETFILEHEADER;   //First task: search for TZX header (default)
  checkForEXT(filename);         //NB: file extension can override currentTask
  currentBlockTask = READPARAM;  //First block task is to read in parameters
  ayPass = 0;                    //Reset AY PASS flag
  ayHeaderPos = HDRSTART;        //Start reading from position 1 -> 0x13 [0x00]
  blockChecksum = 0;             //reset block chksum byte for AY loading routine
  bID15switch = false;           //reset ID15switch
  flushCount = 255;              //End of file buffer flush
  procSpread = 0;
  bEndOfFile = false;
  bOutState = LOW;
  AUDIO_OUT(LOW);
  Timer1.setPeriod(1000);        //set 1ms wait at start of a file.
}


void TZXStop(void) {
  Timer1.stop();
  bIsRunning = false;
  curFileClose();
  bytesRead = 0;        // reset read bytes
}


void TZXPause(void) {
  bIsRunning = !bPaused;
}


void TZXLoop(void) {
  bIsRunning = !bPaused;
  // NB: TZXLoop must be called faster/more often than the ISR is reading a new period from the buffer.
  // byte reads are atomic, and we only ever read wavePos, so we don't need to protect from interrupts
  if ((wavePos ^ fillPos) & WBUFSLICE) {
    // (we do double-buffer flipping with this statement, it just requires less computation)
    // keep filling our slice until it is full
    TZXProcess();  //generate the next period to add to the buffer
    if (currentPeriod) {
      if (!bitRead(currentPeriod, WAV_BIT_PAUSE)) {
        if (!bID15switch) {
          // ID15 blocks are 'direct recording' (with a sample rate of 22.05 or 44.1kHz)
          // during this processing we already have an outState (audio level) set in currentPeriod bit14
          bOutState = !bOutState; // not pause, and not ID15 processing: toggle output pin level
          if (bOutState) bitSet(currentPeriod, WAV_BIT_LEVEL);
        }
      } // else (pause), the audio level is already in bit14
      waveBuffer[fillPos] = currentPeriod;  // add period to the waveBuffer (inactive part, so no interrupt guard required)
      fillPos++;
      fillPos &= WBUFMASK; // keep fillPos inside our waveBuffer
    } // else there was no processor for this ID, and we silently skip it
  } else {
    if (fillPos & (WBUFSLICE - 1)) {
      // normally, the fill position 'waits' at the start of the buffer
      // if it is anywhere inside, the ISR was too fast and we have a buffer overrun
      msgErrOverrun();
    }
    if (bPreFill) { // initial buffer fill
      bPreFill = false;
      wavePos = 0;
      bIsRunning = true;
    }
    // Display updates are expensive (especially on bitmapped output devices).
    // Therefore we alternate between tapeCounter and filePercent updates.
    procSpread++;
    if (procSpread & 1) {
      if (!bPaused && (filePercent < 100)) {
        tapeCounterUpdate();
      }
    } else {
      byte newpct = (100 * bytesRead) / filesize;
      if (newpct != filePercent) {
        filePercent = newpct;
        filePercentPrint();
      }
    }
  }
}


static void TZXProcess() {
  bID15WantLevelSet = false;
  currentPeriod = 0;

  // these three tasks are exclusive
  if (currentTask == GETFILEHEADER) {
    ReadTZXHeader();
    currentTask = GETID;
  } else if (currentTask == GETAYHEADER) {
    ReadAYHeader();
    currentTask = PROCESSID;
  } else if (currentTask == GETUEFHEADER) {
    ReadUEFHeader();
    currentTask = GETCHUNKID;
  }

  if (currentTask == GETCHUNKID) {
    UEF_GetChunkID();
  }

  if (currentTask == PROCESSCHUNKID) {
    UEF_ProcessChunkID();
  }

  if (currentTask == GETID) {
    TZX_GetID();
  }

  if (currentTask == PROCESSID) {
    TZX_ProcessID();
  }

#ifdef TZXDEBUG
  Serial.println(currentPeriod, 10);
#endif
#ifdef ZERO_PERIOD_DEBUG
  if (!currentPeriod) {
    Serial.print("zero_period: fP=");
    Serial.print(fillPos);
    Serial.print(" - bytesRead=");
    Serial.print(bytesRead);
    Serial.print(" - Task: 0x");
    Serial.print(currentTask, 16);
    Serial.print(" - ID: 0x");
    Serial.print(currentID, 16);
    Serial.print(" - BlockTask: 0x");
    Serial.println(currentBlockTask, 16);
  }
#endif // ZERO_PERIOD_DEBUG
}


void TZX_GetID(void) { //grab 1 byte ID
  if (ReadByte(bytesRead)) {
    currentID = outByte;
  } else {
    currentID = IDEOF;
  }
  //reset data block values
  currentBit = 0;
  currentPass = 0;
  //set current task to PROCESSID
  currentTask = PROCESSID;
  currentBlockTask = READPARAM;
}


void TZX_ProcessID(void) {

  switch (currentID) {

    case TAP:  //Pure Tap file block
    case ID10:  //Process ID10 - Standard Block
      switch (currentBlockTask) {
        case READPARAM:
          if (currentID == TAP) {
            // - TAP uses a standard length of 1 second
            nextPauseLength = PAUSELENGTH;
          } else {
            // - TZX gets the pause length from the file
            if (ReadWord(bytesRead)) {
              nextPauseLength = outWord;
            }
          }
          if (ReadWord(bytesRead)) {
            bytesToRead = outWord + 1; // data length plus checksum byte
          }
          if (ReadByte(bytesRead)) {
            // first byte is usually 00 or FF marker
            // TODO: some JupiterAce TAP files don't have this byte, maybe we can add an option to 'synthesize' it.
            pilotPulses = outByte ? PILOTNUMBERH : PILOTNUMBERL;
            if (currentID == TAP) {
              pilotPulses++; // TODO: check if we really need this
              if (cfgJupiterTAP) {
                if (ayPass == 7) {
                  // we had a header before and now read the data block
                  ayPass = 6;
                } else if (outByte == 0 && bytesToRead == 0x1B) {
                  // this is a header and we need to correct it
                  // NB: 'frogger.tap' has a header length of 0x19 (bytesToRead == 0x1A)
                  ayPass = 8;
                }
              }
            }
            bytesRead--; // we just peek its value
          }
          pilotLength = PILOTLENGTH;
          sync1Length = SYNCFIRST;
          sync2Length = SYNCSECOND;
          zeroPulse = ZEROPULSE;
          onePulse = ONEPULSE;
          currentBlockTask = PILOT;
          usedBitsInLastByte = 8;
          break;

        default:
          StandardBlock();
          break;
      }
      break;

    case ID11:
      //Process ID11 - Turbo Tape Block
      switch (currentBlockTask) {
        case READPARAM:
          if (ReadWord(bytesRead)) {
            pilotLength = TickToUs(outWord);
          }
          if (ReadWord(bytesRead)) {
            sync1Length = TickToUs(outWord);
          }
          if (ReadWord(bytesRead)) {
            sync2Length = TickToUs(outWord);
          }
          if (ReadWord(bytesRead)) {
            zeroPulse = TickToUs(outWord);
          }
          if (ReadWord(bytesRead)) {
            onePulse = TickToUs(outWord);
          }
          if (ReadWord(bytesRead)) {
            pilotPulses = outWord;
          }
          if (ReadByte(bytesRead)) {
            usedBitsInLastByte = outByte;
          }
          if (ReadWord(bytesRead)) {
            nextPauseLength = outWord;
          }
          if (ReadLong(bytesRead)) {
            bytesToRead = outLong + 1;
          }
          currentBlockTask = PILOT;
          break;
        default:
          StandardBlock();
          break;
      }
      break;

    case ID12:
      //Process ID12 - Pure Tone Block
      if (currentBlockTask == READPARAM) {
        if (ReadWord(bytesRead)) {
          pilotLength = TickToUs(outWord);
        }
        if (ReadWord(bytesRead)) {
          pilotPulses = outWord;
        }
        currentBlockTask = PILOT;
      } else {
        PureToneBlock();
      }
      break;

    case ID13:
      //Process ID13 - Sequence of Pulses
      if (currentBlockTask == READPARAM) {
        if (ReadByte(bytesRead)) {
          seqPulses = outByte;
        }
        currentBlockTask = DATA;
      } else {
        PulseSequenceBlock();
      }
      break;

    case ID14:
      //process ID14 - Pure Data Block
      if (currentBlockTask == READPARAM) {
        if (ReadWord(bytesRead)) {
          zeroPulse = TickToUs(outWord);
        }
        if (ReadWord(bytesRead)) {
          onePulse = TickToUs(outWord);
        }
        if (ReadByte(bytesRead)) {
          usedBitsInLastByte = outByte;
        }
        if (ReadWord(bytesRead)) {
          nextPauseLength = outWord;
        }
        if (ReadLong(bytesRead)) {
          bytesToRead = outLong + 1;
        }
        currentBlockTask = DATA;
      } else {
        PureDataBlock();
      }
      break;

    case ID15:
      //process ID15 - Direct Recording
      if (currentBlockTask == READPARAM) {
        if (ReadWord(bytesRead)) {
          //Number of T-states per sample (bit of data) 79 or 158 - 22.6757uS for 44.1KHz
          directRecordingPeriod = TickToUs(outWord);
        }
        if (ReadWord(bytesRead)) {
          //Pause after this block in milliseconds
          nextPauseLength = outWord;
        }
        if (ReadByte(bytesRead)) {
          //Used bits in last byte (other bits should be 0)
          usedBitsInLastByte = outByte;
        }
        if (ReadLong(bytesRead)) {
          // Length of samples' data
          bytesToRead = outLong + 1;
        }
        currentBlockTask = DATA;
      } else {
        currentPeriod = directRecordingPeriod;
        bID15WantLevelSet = true;  //bitSet(currentPeriod, WAV_BIT_LEVEL);
        DirectRecording();
      }
      break;

    case ID19:
      //Process ID19 - Generalized data block
      switch (currentBlockTask) {
        case READPARAM:
          if (ReadDword(bytesRead)) {
            //bytesToRead = outLong;
          }
          if (ReadWord(bytesRead)) {
            //Pause after this block in milliseconds
            nextPauseLength = outWord;
          }
          bytesRead += 86;  // skip until DataStream filename
          //bytesToRead += -88 ;    // pauseLength + SYMDEFs
          currentBlockTask = DATA;
          break;

        case DATA:
          ZX8081DataBlock();
          break;
      }
      break;

    case ID20:
      //process ID20 - Pause Block
      if (ReadWord(bytesRead)) {
        if (outWord) {
          workPauseLength = outWord;
          currentID = IDPAUSE;
          currentBlockTask = READPARAM;
        } else {
          currentTask = GETID;
        }
      }
      break;

    case ID21:
      //Process ID21 - Group Start
      if (ReadByte(bytesRead)) {
        bytesRead += outByte; // skip
      }
      currentTask = GETID;
      break;

    case ID22:
      //Process ID22 - Group End
      currentTask = GETID;
      break;

    case ID24:
      //Process ID24 - Loop Start
      if (ReadWord(bytesRead)) {
        loopCount = outWord;
        loopStart = bytesRead;
      }
      currentTask = GETID;
      break;

    case ID25:
      //Process ID25 - Loop End
      loopCount--;
      if (loopCount) {
        bytesRead = loopStart;
      }
      currentTask = GETID;
      break;

    case ID2A:
      //Skip ID2A - Stop the tape if in 48K mode//
      bytesRead += 4;
      currentTask = GETID;
      break;

    case ID2B:
      //Skip ID2B - Set signal level//
      bytesRead += 5;
      currentTask = GETID;
      break;

    case ID30:
      //Skip ID30 - Text Description
      //Block Skipped until larger screen used
      if (ReadByte(bytesRead)) {
        //Show info on screen - removed until bigger screen used
        //byte j = outByte;
        //for(byte i=0; i<j; i++) {
        //  if(ReadByte(bytesRead)) {
        //    lcd.print(char(outByte));
        //  }
        //}
        bytesRead += outByte;
      }
      currentTask = GETID;
      break;

    case ID31:
      //Process ID31 - Message block
      if (ReadByte(bytesRead)) {
        // displayTime = outByte;
      }
      if (ReadByte(bytesRead)) {
        bytesRead += outByte;
      }
      currentTask = GETID;
      break;

    case ID32:
      //Skip ID32 - Archive Info
      //Block Skipped until larger screen used
      if (ReadWord(bytesRead)) {
        bytesRead += outWord;
      }
      currentTask = GETID;
      break;

    case ID33:
      //Skip ID 33 - Hardware type
      //Block Skipped until larger screen used
      if (ReadByte(bytesRead)) {
        bytesRead += (long(outByte) * 3);
      }
      currentTask = GETID;
      break;

    case ID35:
      //Skip ID35 - Custom Info Block
      //Block Skipped
      bytesRead += 0x10; // ASCII Identification String
      if (ReadDword(bytesRead)) {
        bytesRead += outLong; // length of custom info block
      }
      currentTask = GETID;
      break;

    case ID4B:
      //Process ID4B - Kansas City Block (MSX specific implementation only)
      switch (currentBlockTask) {
        case READPARAM:
          if (ReadDword(bytesRead)) {  // Data size to read
            bytesToRead = outLong - 12;
          }
          if (ReadWord(bytesRead)) {  // Pause after block in ms
            nextPauseLength = outWord;
          }
          if (bTSXSpeedUp) { //Fixed speedup baudrate, reduced pilot duration
            pilotPulses = 10000;
            bytesRead += 10; // skip over file settings
            switch (cfgBaudrate) {
              case 1200:
                pilotLength = onePulse = TickToUs(729);
                zeroPulse = TickToUs(1458);
                break;
              case 2400:
                pilotLength = onePulse = TickToUs(365);
                zeroPulse = TickToUs(730);
                break;
              case 3600:
                pilotLength = onePulse = TickToUs(243);
                zeroPulse = TickToUs(486);
                break;
              case 3760:
                pilotLength = onePulse = TickToUs(233);
                zeroPulse = TickToUs(466);
                break;
            }
          } else { // not bTSXSpeedUp
            if (ReadWord(bytesRead)) {  // T-states each pilot pulse
              pilotLength = TickToUs(outWord);
            }
            if (ReadWord(bytesRead)) {  // Number of pilot pulses
              pilotPulses = outWord;
            }
            if (ReadWord(bytesRead)) {  // T-states 0 bit pulse
              zeroPulse = TickToUs(outWord);
            }
            if (ReadWord(bytesRead)) {  // T-states 1 bit pulse
              onePulse = TickToUs(outWord);
            }
            ReadWord(bytesRead);
          } // (not) TSX_SPEEDUP

          currentBlockTask = PILOT;
          break;

        case PILOT:
          //Start with Pilot Pulses
          if (pilotPulses) {
            currentPeriod = pilotLength;
            pilotPulses--;
          } else {
            currentBlockTask = DATA;
          }
          break;

        case DATA:
          //Data playback
          writeData4B();
          break;

        case PAUSE:
          //Close block with a pause
          workPauseLength = nextPauseLength;
          currentID = IDPAUSE;
          currentBlockTask = READPARAM;
          break;
      }
      break;

    case ZXP:
      switch (currentBlockTask) {
        case READPARAM:
          nextPauseLength = PAUSELENGTH * 5;  // Standard 5 sec pause
          currentChar = 0;
          currentBlockTask = PILOT;
          break;

        case PILOT:
          ZX81FilenameBlock();
          break;

        case DATA:
          ZX8081DataBlock();
          break;
      }
      break;

    case ZXO:
      switch (currentBlockTask) {
        case READPARAM:
          nextPauseLength = PAUSELENGTH * 5;  // Standard 5 sec pause
          currentBlockTask = DATA;
          break;

        case DATA:
          ZX8081DataBlock();
          break;
      }
      break;

    case AYO:  //AY File - Pure AY file block - no header, must emulate it
      switch (currentBlockTask) {
        case READPARAM:
          nextPauseLength = PAUSELENGTH;  // Standard 1 sec pause
          // here we must generate the TAP header which in pure AY files is missing.
          // This was done with a DOS utility called FILE2TAP which does not work under recent 32bit OSs (only using DOSBOX).
          // TAPed AY files begin with a standard 0x13 0x00 header (0x13 bytes to follow) and contain the
          // name of the AY file (max 10 bytes) which we will display as "ZXAYFile " followed by the
          // length of the block (word), checksum plus 0xFF to indicate next block is DATA.
          // 13 00[00 03(5A 58 41 59 46 49 4C 45 2E 49)1A 0B 00 C0 00 80]21<->[1C 0B FF<AYFILE>CHK]
          //if(ayHeaderPos==1) {
          //bytesToRead = 0x13-2; // 0x13 0x0 - TAP Header minus 2 (FLAG and CHKSUM bytes) 17 bytes total
          //}
          pilotPulses = (ayHeaderPos == HDRSTART) ? (PILOTNUMBERL + 1) : (PILOTNUMBERH + 1);
          pilotLength = PILOTLENGTH;
          sync1Length = SYNCFIRST;
          sync2Length = SYNCSECOND;
          zeroPulse = ZEROPULSE;
          onePulse = ONEPULSE;
          currentBlockTask = PILOT;  // Now send pilot, SYNC1, SYNC2 and DATA (writeheader() from String Vector on 1st pass then writeData() on second)
          if (ayHeaderPos == HDRSTART) {
            ayPass = 1;       // Set AY TAP data read flag only if first run
          }
          if (ayPass == 2) {  // If we have already sent TAP header
            blockChecksum = 0;
            bytesRead = 0;
            bytesToRead = ayBlkLen + 2;  // set length of file to be read plus data byte and CHKSUM (and 2 block LEN bytes)
            ayPass = 5;                  // reset flag to read from file and output header 0xFF byte and end chksum
          }
          usedBitsInLastByte = 8;
          break;

        default:
          StandardBlock();
          break;
      }
      break;

    case ORIC:
      ORIC_ProcessBlock();
      break;

    case IDPAUSE:
      switch (currentBlockTask) {
        case READPARAM: // make a final 1.5ms - if we have the time for it
          if (workPauseLength > 2) {
            // re-compute workPauseLength from 1000 multiplyer to 1024 for ISR
            // subtract the 1500 usecs we already pause before
            // add 512 for rounding up, if required, i.e. -988 = (-1500 + 512)
            workPauseLength = ((long(workPauseLength) * 1000L) - 988) >> 10;
            currentPeriod = 1500; // microseconds
          }
          currentBlockTask = DATA;
          break;

        case DATA: // loop through pause definitions
          if (workPauseLength) {
            // we break up pause lengths in WAV_MSK_PERIOD chunks
            if (workPauseLength > WAV_MSK_PERIOD) {
              currentPeriod = WAV_MSK_PERIOD;
              workPauseLength -= WAV_MSK_PERIOD;
            } else {
              currentPeriod = workPauseLength;
              workPauseLength = 0;
            }
            if (cfgFlipPolarity) {
              // "Gremlin loader" == "invert polarity of the 'pause' audio level"
              bitSet(currentPeriod, WAV_BIT_LEVEL);
            } // else do not set WAV_BIT_LEVEL (bit14), making it LOW
            bitSet(currentPeriod, WAV_BIT_PAUSE);
          } else {
            currentTask = GETID;
            if (bEndOfFile) {
              currentID = IDEOF;
            }
          }
          break;

        default:
          currentBlockTask = READPARAM; // paranoia
          break;
      }
      break;

    case IDEOF:
      //Handle end of file
      if (flushCount) {
        currentPeriod = 32; // was 32767 (usec), but we want a pause, and LOW output
        bitSet(currentPeriod, WAV_BIT_PAUSE);
        flushCount--;
      } else {
        stopFile();
        return;
      }
      break;

    default:
      //ID Not Recognised - Fall back if non TZX file or unrecognised ID occurs
      debugPrintTZXInfo();
      stopFile();
      break;
  }
}


void StandardBlock() { //Standard Block Playback
  switch (currentBlockTask) {
    case PILOT: //Start with Pilot Pulses
      currentPeriod = pilotLength;
      pilotPulses--;
      if (pilotPulses == 0) {
        currentBlockTask = SYNC1;
      }
      break;
    case SYNC1: //First Sync Pulse
      currentPeriod = sync1Length;
      currentBlockTask = SYNC2;
      break;
    case SYNC2: //Second Sync Pulse
      currentPeriod = sync2Length;
      currentBlockTask = DATA;
      break;
    case DATA: //Data Playback
      if (ayPass == 1) {
        writeHeader();  // write TAP Header data from String Vector
      } else {
        writeData();  // Check if we are playing from file or Vector String and we need to send first 0xFF byte or checksum byte at EOF
      }
      break;
    case PAUSE: // Close block with a pause
      if ((currentID == TAP) || (currentID == AYO)) {
        // We need to stay within our currentID, that's why we cannot just re-use IDPAUSE instead.
        if (currentPass == 0) { // see IDPAUSE / READPARAM
          workPauseLength = ((long(nextPauseLength) * 1000L) - 988) >> 10;
          if (workPauseLength < 5) {
            workPauseLength = 5;
          }
          currentPeriod = 1500; // microseconds, keep polarity
          currentPass++;
        } else { // see IDPAUSE / DATA
          if (workPauseLength > WAV_MSK_PERIOD) {
            currentPeriod = WAV_MSK_PERIOD;
            workPauseLength -= WAV_MSK_PERIOD;
          } else {
            currentPeriod = workPauseLength;
            if (!currentPeriod) {
              currentPeriod++;  // at least 1 ms
            }
            workPauseLength = 0;
            currentBlockTask = READPARAM;
            currentPass = 0;
          }
          if (cfgFlipPolarity) {
            bitSet(currentPeriod, WAV_BIT_LEVEL);
          }
          bitSet(currentPeriod, WAV_BIT_PAUSE);
        }
      } else {
        workPauseLength = nextPauseLength;
        currentID = IDPAUSE;
        currentBlockTask = READPARAM;
      }
      if (bEndOfFile) {
        currentID = IDEOF;
      }
      break;
  }
}

void PureToneBlock() { //Pure Tone Block - Long string of pulses with the same length
  currentPeriod = pilotLength;
  pilotPulses--;
  if (pilotPulses == 0) {
    currentTask = GETID;
  }
}

void PulseSequenceBlock() { //Pulse Sequence Block - String of pulses each with a different length
  //Mainly used in speedload blocks
  if (ReadWord(bytesRead)) {
    currentPeriod = TickToUs(outWord);
    // not required: pulse lengths will normally not have bits 14 and 15 set
    // currentPeriod &= WAV_MSK_PERIOD;
  }
  seqPulses--;
  if (seqPulses == 0) {
    currentTask = GETID;
  }
}

void PureDataBlock() { //Pure Data Block - Data & pause only, no header, sync
  switch (currentBlockTask) {
    case DATA:
      writeData();
      break;
    case PAUSE:
      workPauseLength = nextPauseLength;
      currentID = IDPAUSE;
      currentBlockTask = READPARAM;
      break;
  }
}

void writeData4B() {
  //Convert byte (4B Block) from file into string of pulses.  One pulse per pass
  if (currentBit > 0) {  // Continue with current byte
    if (currentBit == 11) {  // Start bit (0)
      currentPeriod = zeroPulse;
      currentPass++;
      if (currentPass == 2) {
        currentBit--;
        currentPass = 0;
      }
    } else if (currentBit <= 2) {  // two Stop bits (1)
      currentPeriod = onePulse;
      currentPass++;
      if (currentPass == 4) {
        currentBit--;
        currentPass = 0;
      }
    } else {  // eight Data bits
      bool dataBit = currentByte & 1;
      currentPeriod = dataBit ? onePulse : zeroPulse;
      currentPass++;
      // 1 bit = 4 pulses, 0-bit = 2 pulses
      if ((dataBit && currentPass == 4) || (!dataBit && currentPass == 2)) {
        currentByte >>= 1;
        currentBit--;
        currentPass = 0;
      }
    }
  } else { // (currentBit == 0)
    if (bytesToRead) {  // (currentBit == 0 && bytesToRead != 0)
      // Read new byte
      if (ReadByte(bytesRead)) {
        bytesToRead--;
        currentByte = outByte;
        currentBit = 11; //1 start bit, 8 data bits, 2 stop bits
        currentPass = 0;
      } else {  //End of file
        currentID = IDEOF;
        return;
      }
    } else {  //(bytesToRead == 0 && currentBit == 0)
      // End of block
      workPauseLength = nextPauseLength;
      currentBlockTask = PAUSE;
    }
  }
}


void ZX81FilenameBlock() {
  if (currentBit == 0) {  //Check for byte end/first byte
    currentByte = pgm_read_byte(ZX81Filename + currentChar);
    currentChar++;
    if (currentChar == 10) {
      currentBlockTask = DATA;
      return;
    }
    currentBit = 9;
    currentPass = 0;
  }
  ZX80ByteWrite();
}

void ZX8081DataBlock() {
  if (currentBit == 0) {  //Check for byte end/first byte
    if (ReadByte(bytesRead)) {  //Read in a byte
      currentByte = outByte;
      bytesToRead--;
    } else { // EOF
      workPauseLength = nextPauseLength;
      currentID = IDPAUSE;
      currentBlockTask = READPARAM;
    }
    currentBit = 9;
    currentPass = 0;
  }
  ZX80ByteWrite();
}

void ZX80ByteWrite() {
  if (cfgTurboBoost) {
    currentPeriod = ZX80TURBOPULSE;
    if (currentPass == 1) {
      currentPeriod = ZX80TURBOBITGAP;
    }
  } else {
    currentPeriod = ZX80PULSE;
    if (currentPass == 1) {
      currentPeriod = ZX80BITGAP;
    }
  }
  if (currentPass == 0) {
    currentPass = (currentByte & 0x80) ? 19 : 9;
    currentByte <<= 1;  //Shift along to the next bit
    currentBit--;
    currentPeriod = 0;
  }
  currentPass--;
}

void writeData() {
  //Convert byte from file into string of pulses.  One pulse per pass
  if (currentBit == 0) {  //Check for byte end/first byte
    if (ReadByte(bytesRead)) { // get next byte
      currentByte = outByte;
      if (ayPass == 8) { // Jupiter Ace Fix for missing HEADER block type byte
        ayPass = 7;          // no other changes in this data block
        currentByte = 0x00;  // HEADER block type
        bytesRead--;         // undo read
        bytesToRead++;       // undo read
      } else if (ayPass == 6) { // Jupiter Ace Fix for missing DATA block type byte
        ayPass = 0;          // no other changes, reset flag
        currentByte = 0xFF;  // DATA block type
        blockChecksum = 0xFF;// reset checksum, subtract block type from checksum
        bytesRead--;         // undo read
        bytesToRead++;       // undo read
      } else if (ayPass == 5) {
        ayPass = 4;          // set Checksum flag to be sent when EOF reached
        currentByte = 0xFF;  // insert missing DATA block type byte for AY file
        blockChecksum = 0xFF;// reset checksum, subtract block type from checksum
        bytesRead--;         // undo read
        bytesToRead += 2;    // undo read for simulated 0xFF plus add 1 byte for checksum at the block end
      }
      bytesToRead--;
      blockChecksum = blockChecksum ^ currentByte;  //Update block checksum
      if (bytesToRead == 0) {        //Check for end of data block
        bytesRead--;                 //rewind a byte if we've reached the end
        if (nextPauseLength) {       //Search for next ID if there is no pause
          currentBlockTask = PAUSE;  //Otherwise start the pause
        } else {
          currentTask = GETID;
        }
        return;  // exit
      }
    } else {  // End-Of-File
      if (ayPass == 4) { // Check if need to send checksum
        currentByte = blockChecksum;
        bytesToRead++;            // add one byte to read
        ayPass = 0;               // Reset state to end block after this byte
      } else {
        bEndOfFile = true;
        if (nextPauseLength) {
          currentBlockTask = PAUSE;
          currentPass = 0; // we re-use this to check where we are in the pause processing
        } else {
          currentTask = GETID;
        }
        return;  // return here if normal TAP or TZX
      }
    } // eof
    //If we're not reading the last byte play all 8 bits
    //Otherwise only play back the bits needed
    currentBit = (bytesToRead == 1) ? usedBitsInLastByte : 8;
    currentPass = 0;
#ifdef TZXDEBUG
    Serial.print((currentByte & 0xF0) ? "0x" : "0x0");
    Serial.print(currentByte, 16);
    Serial.print((blockChecksum & 0xF0) ? " cs=0x" : " cs=0x0");
    Serial.print(blockChecksum, 16);
    Serial.print(" cnt=");
    Serial.println(bytesToRead, 10);
#endif //  TZXDEBUG
  } // (currentBit == 0)
  currentPeriod = (currentByte & 0x80) ? onePulse : zeroPulse;
  currentPass++;  //Data is played as 2 x pulses
  if (currentPass == 2) {
    currentByte <<= 1;  //Shift along to the next bit
    currentBit--;
    currentPass = 0;
  }
}

void writeHeader() {
  //Convert byte from HDR Vector String into string of pulses and calculate checksum. One pulse per pass
  if (currentBit == 0) {  //Check for byte end/new byte
    if (ayHeaderPos == 19) {   // If we've reached end of header block, send the checksum byte
      currentByte = blockChecksum;
      ayPass = 2;                // set flag to Stop playing from header in RAM
      currentBlockTask = PAUSE;  // we've finished outputting the TAP header so now PAUSE and send DATA block normally from file
      return;
    }
    ayHeaderPos++;           // increase header string vector pointer
    if (ayHeaderPos < 20) {  //Read a byte until we reach end of tap header
      //currentByte = TAPHdr[ayHeaderPos];
      currentByte = pgm_read_byte(TAPHdr + ayHeaderPos);
      if (ayHeaderPos == 13) {  // insert calculated block length minus LEN bytes
        currentByte = lowByte(ayBlkLen - 3);
      } else if (ayHeaderPos == 14) {
        currentByte = highByte(ayBlkLen);
      }
      blockChecksum = blockChecksum ^ currentByte;  // Keep track of Chksum
      currentBit = 8;
    } else {
      currentBit = usedBitsInLastByte;  //Otherwise only play back the bits needed
    }
    currentPass = 0;
  }  // (currentBit == 0)
  currentPeriod = (currentByte & 0x80) ? onePulse : zeroPulse;
  currentPass++;  //Data is played as 2 x pulses
  if (currentPass == 2) {
    currentByte <<= 1;  //Shift along to the next bit
    currentBit--;
    currentPass = 0;
  }
}  // End writeHeader()

bool ReadByte(unsigned long pos) {
  //Read 1 byte from the file, and move file position on one if successful
  byte out[1];
  int i = 0;
  if (seekPosition(pos)) {
    i = readBuf(out, 1);
    if (i == 1)
      bytesRead++;
  }
  outByte = out[0];
  //blockChecksum = blockChecksum ^ out[0];
  return (i == 1);
}

bool ReadWord(unsigned long pos) {
  //Read 2 bytes from the file, and move file position on two if successful
  byte out[2];
  int i = 0;
  if (seekPosition(pos)) {
    i = readBuf(out, 2);
    if (i == 2)
      bytesRead += 2;
  }
  outWord = word(out[1], out[0]);
  //blockChecksum = blockChecksum ^ out[0] ^ out[1];
  return (i == 2);
}

bool ReadLong(unsigned long pos) {
  //Read 3 bytes from the file, and move file position on three if successful
  byte out[3];
  int i = 0;
  if (seekPosition(pos)) {
    i = readBuf(out, 3);
    if (i == 3)
      bytesRead += 3;
  }
  outLong = ((unsigned long)word(out[2], out[1]) << 8) | out[0];
  //outLong = (word(out[2],out[1]) << 8) | out[0];
  //blockChecksum = blockChecksum ^ out[0] ^ out[1] ^ out[2];
  return (i == 3);
}

bool ReadDword(unsigned long pos) {
  //Read 4 bytes from the file, and move file position on four if successful
  byte out[4];
  int i = 0;
  if (seekPosition(pos)) {
    i = readBuf(out, 4);
    if (i == 4)
      bytesRead += 4;
  }
  outLong = ((unsigned long)word(out[3], out[2]) << 16) | word(out[1], out[0]);
  //outLong = (word(out[3],out[2]) << 16) | word(out[1],out[0]);
  //blockChecksum = blockChecksum ^ out[0] ^ out[1] ^ out[2] ^ out[3];
  return (i == 4);
}

void ReadTZXHeader() {
  //Read and check first 10 bytes for a TZX header
  byte tzxHeader[11];
  int i = 0;

  if (seekPosition(0)) {
    i = readBuf(tzxHeader, 10);
    if (memcmp_P(tzxHeader, TZXTape, 7) != 0) {
      printtextF(PSTR("Not TZXTape"), 1);
      TZXStop();
    }
  } else {
    msgErrFileRead();
  }
  bytesRead = 10;
}

void ReadAYHeader() {
  //Read and check first 8 bytes for a TZX header
  byte ayHeader[9];
  if (seekPosition(0)) {
    (void) readBuf(ayHeader, 8);
    if (memcmp_P(ayHeader, AYFile, 8) != 0) {
      printtextF(PSTR("Not AY File"), 1);
      TZXStop();
    }
  } else {
    msgErrFileRead();
  }
  bytesRead = 0;
  ayBlkLen = filesize + 3;  // add 3 file header, data byte and chksum byte to file length
}

void DirectRecording() {
  //Direct Recording - Output bits based on specified sample rate (Ticks per clock) either 44.1KHz or 22.05
  switch (currentBlockTask) {
    case DATA:
      writeSampleData();
      break;
    case PAUSE:
      workPauseLength = nextPauseLength;
      currentID = IDPAUSE;
      currentBlockTask = READPARAM;
      break;
  }
}

void writeSampleData() {
  //Convert byte from file into string of pulses.  One pulse per pass
  bID15switch = true; // only called from DirectRecording which is called from ID15 processing
  if (currentBit == 0) {  //Check for byte end/first byte
    if (ReadByte(bytesRead)) {  //Read in a byte
      currentByte = outByte;
      bytesToRead--;
      if (bytesToRead == 0) {    //Check for end of data block
        bytesRead--;             //rewind a byte if we've reached the end
        if (nextPauseLength) {  //Search for next ID if there is no pause
          currentBlockTask = PAUSE;  //Otherwise start the pause
        } else {
          currentTask = GETID;
        }
        return;
      }
    } else {
      bEndOfFile = true;
      if (nextPauseLength) {
        currentBlockTask = PAUSE;
      } else {
        currentTask = GETID;
      }
      return;
    }
    //if we're reading the last byte only play back the bits needed, otherwise play all 8 bits
    currentBit = (bytesToRead == 1) ? usedBitsInLastByte : 8;
    currentPass = 0;
  }

  if (bID15WantLevelSet) {
    if (currentByte & 0x80) {
      bitSet(currentPeriod, WAV_BIT_LEVEL);
    }
    currentPass += 2;
  } else {
    //Set next period depending on value of bit 0
    currentPeriod = (currentByte & 0x80) ? onePulse : zeroPulse;
    currentPass += 1;
  }

  if (currentPass == 2) {
    currentByte <<= 1;  //Shift along to the next bit
    currentBit--;
    currentPass = 0;
  }
}

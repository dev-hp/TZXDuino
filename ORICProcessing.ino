// ORIC Processing
#include "userconfig.h"
#include "TZXDuino.h"

void ORIC_ProcessBlock(void) {
  switch (currentBlockTask) {
    case READPARAM:  // currentBit == 0 and count == 255
    case SYNC1:
      if (currentBit > 0)
        OricBitWrite();
      else {
        ReadByte(bytesRead);
        currentByte = outByte;
        currentBit = 11;
        bitChecksum = 0;
        bLastByte = false;
        if (currentByte == 0x16) {
          flushCount--;
        } else {
          //TODO: check if missing "count = 1;" here
          // at the moment the header will be extended to 255 bytes
          currentBit = 0;  //0x24
          currentBlockTask = SYNC2;
        }
      }
      break;

    case SYNC2:
      if (currentBit > 0)
        OricBitWrite();
      else {
        if (flushCount) {
          currentByte = 0x16;
          currentBit = 11;
          bitChecksum = 0;
          bLastByte = false;
          flushCount--;
        } else {
          flushCount = 1;  //0x24
          currentBlockTask = SYNCLAST;
        }
      }
      break;

    case SYNCLAST:
      if (currentBit > 0)
        OricBitWrite();
      else {
        if (flushCount) {
          currentByte = 0x24;
          currentBit = 11;
          bitChecksum = 0;
          bLastByte = false;
          flushCount--;
        } else {
          flushCount = 9;
          bLastByte = false;
          currentBlockTask = HEADER;
        }
      }
      break;

    case HEADER:
      if (currentBit > 0)
        OricBitWrite();
      else {
        if (flushCount) {
          ReadByte(bytesRead);
          currentByte = outByte;
          currentBit = 11;
          bitChecksum = 0;
          bLastByte = false;
          if (flushCount == 5)
            bytesToRead = 256 * outByte;
          else if (flushCount == 4)
            bytesToRead += (outByte + 1);
          else if (flushCount == 3)
            bytesToRead -= (256 * outByte);
          else if (flushCount == 2)
            bytesToRead -= outByte;
          flushCount--;
        } else
          currentBlockTask = NAME;
      }
      break;

    case NAME:
      if (currentBit > 0)
        OricBitWrite();
      else {
        ReadByte(bytesRead);
        currentByte = outByte;
        currentBit = 11;
        bitChecksum = 0;
        bLastByte = false;
        if (currentByte == 0x00) {
          flushCount = 1;
          currentBit = 0;
          currentBlockTask = NAMELAST;
        }
      }
      break;

    case NAMELAST:
      if (currentBit > 0)
        OricBitWrite();
      else {
        if (flushCount) {
          currentByte = 0x00;
          currentBit = 11;
          bitChecksum = 0;
          bLastByte = false;
          flushCount--;
        } else {
          flushCount = 100;
          bLastByte = false;
          currentBlockTask = GAP;
        }
      }
      break;

    case GAP:
      if (flushCount) {
        currentPeriod = ORICONEPULSE;
        flushCount--;
      } else {
        currentBlockTask = DATA;
      }
      break;

    case DATA:
      OricDataBlock();
      break;

    case PAUSE:
      //currentPeriod = 100; // 100ms pause
      //bitSet(currentPeriod, 15);
      if (flushCount) {
        currentPeriod = 32769;
        flushCount--;
      } else {
        flushCount = 255;
        currentBlockTask = SYNC1;
      }
      break;
  }
} // ORIC_ProcessBlock


void OricDataBlock(void) {
  //Convert byte from file into string of pulses.  One pulse per pass
  if (currentBit == 0) {                      //Check for byte end/first byte
    if (ReadByte(bytesRead)) {       //Read in a byte
      currentByte = outByte;
      bytesToRead--;
      bitChecksum = 0;
      if (bytesToRead == 0) {                 //Check for end of data block
        bLastByte = true;
        //if(nextPauseLength==0) {                  //Search for next ID if there is no pause
        //currentTask = IDEOF;
        //} else {
        //currentBlockTask = PAUSE;           //Otherwise start the pause
        //}
        //return;                               // exit
      }
    } else {                      // If we reached the EOF
      bEndOfFile = true;
      workPauseLength = 0;
      flushCount = 255;
      currentID = IDPAUSE;
      //currentBlockTask = GAP;
      //currentTask = IDEOF;
      return;
    }
    currentBit = 11;
    currentPass = 0;
  }
  OricBitWrite();
} // OricDataBlock


void OricBitWrite(void) {
  if (currentBit == 11) { //Start Bit
    //currentPeriod = ORICZEROPULSE;
    if (currentPass == 0) currentPeriod = ORICZEROLOWPULSE;
    if (currentPass == 1) currentPeriod = ORICZEROHIGHPULSE;
  } else if (currentBit == 2) { // Paridad inversa i.e. Impar
    //currentPeriod =  bitChecksum ? ORICONEPULSE : ORICZEROPULSE;
    if (currentPass == 0)  currentPeriod = bitChecksum ? ORICZEROLOWPULSE : ORICONEPULSE;
    if (currentPass == 1)  currentPeriod = bitChecksum ? ORICZEROHIGHPULSE : ORICONEPULSE;
  } else if (currentBit == 1) {
    currentPeriod = ORICONEPULSE;
  } else {
    if (currentByte & 1) {                    //Set next period depending on value of bit 0
      currentPeriod = ORICONEPULSE;
    } else {
      //currentPeriod = ORICZEROPULSE;
      if (currentPass == 0)  currentPeriod = ORICZEROLOWPULSE;
      if (currentPass == 1)  currentPeriod = ORICZEROHIGHPULSE;
    }
  }

  currentPass++;    //Data is played as 2 x pulses for a zero, and 2 pulses for a one

  if (currentPeriod == ORICONEPULSE) {
    // must be a one pulse
    /*    if(currentPass==2) {
          if ((currentBit>2) && (currentBit<11)) {
            bitChecksum ^= 1;
            currentByte >>= 1;                        //Shift along to the next bit
          }
          currentBit--;
          currentPass=0;
          if ((currentBit==0) && bLastByte) {
            //currentTask = GETCHUNKID;
            currentBlockTask = PAUSE;
          }
        } */
    if ((currentBit > 2) && (currentBit < 11) && (currentPass == 2)) {
      bitChecksum ^= 1;
      currentByte >>= 1;                        //Shift along to the next bit
      currentBit--;
      currentPass = 0;
    }
    if ((currentBit == 1) && (currentPass == 6)) {
      currentBit--;
      currentPass = 0;
    }
    if (((currentBit == 2) || (currentBit == 11))  && (currentPass == 2)) {
      currentBit--;
      currentPass = 0;
    }
    if ((currentBit == 0) && bLastByte) {
      //currentTask = GETCHUNKID;
      flushCount = 255;
      currentBlockTask = PAUSE;
    }
  } else {
    // must be a zero pulse
    if (currentPass == 2) {
      if ((currentBit > 2) && (currentBit < 11)) {
        currentByte >>= 1;                        //Shift along to the next bit
      }
      currentBit--;
      currentPass = 0;
      if ((currentBit == 0) && bLastByte) {
        //currentTask = GETCHUNKID;
        flushCount = 255;
        currentBlockTask = PAUSE;
      }
    }

  }
} // OricBitWrite

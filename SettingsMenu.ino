/* 
  TZXDuino Menu Code

  Menu Button (was motor control button) opens menu
  up/down move through menu, play select, stop back

  Menu Options:

  Pause @ Start:
    On, Off

  Jupiter Ace TAP fix:
    On, Off

  Turbo Boost:  Speeds up ZX80/81 TZX/O/P files and Electron UEF files TURN OFF IF NOT USING THESE FILES
    On, Off

  Gremlin Loader  (cfgFlipPolarity)
    On, Off

  MSX Baud Rate: (ID4B - Kansas City Block)
    1200, 2400, 3600, 3670 - highest enabled one will be chosen

  Save settings on exit.
*/

#include "userconfig.h"
#include "Buttons.h"

#define MAX_MENU_LINE_LEN 16
#define MNU_EXIT  char(-1)
#define MNU_LIST  char(0)
#define MNU_ONOFF char(-1)
#define MNU_FLAG  char(-2)

#define I_BACK(x) ((x)*3+0)
#define I_PAR1(x) ((x)*3+1)
#define I_PAR2(x) ((x)*3+2)

void menuSettings() {
  const char *s_menu[] = {
    PSTR("Menu Screen"),     // 0
    PSTR("Pause@Start"),     // 1
    PSTR("JupiterFix"),      // 2
    PSTR("Turbo Boost"),     // 3
    PSTR("Gremlin Load"),    // 4
    PSTR("MSX Baud Rate"),   // 5
    PSTR("1200 Baud"),       // 6
    PSTR("2400 Baud"),       // 7
    PSTR("3600 Baud"),       // 8
    PSTR("3670 Baud"),       // 9
  };
  const char control[] = { // (back position, start, end) or (back position, type, bitPos)
    MNU_EXIT, 1, 5,   // 'Menu Screen' main menu, exit menu flag, submenu = start pos = 1, end pos = 6
    0, MNU_FLAG, 5,  // 'Pause @ Start', exit to main menu, 'ON' and 'OFF' choice, config bit 5
    0, MNU_FLAG, 4,  // 'JupiterTapFix', exit to main menu, 'ON' and 'OFF' choice, config bit 4
    0, MNU_FLAG, 7,  // 'Turbo Boost', exit to main menu, 'ON' and 'OFF' choice, config bit 7
    0, MNU_FLAG, 6,  // 'Gremlin Loader', exit to main menu, 'ON' and 'OFF' choice, config bit 6
    0, 6, 9,          // 'MSX Baud Rate', exit to main menu, sub menu is 1200/2400/3600/3670 Baud selection
    5, MNU_FLAG, 0,   // '1200 Baud', exit to MSX Baud Rate, asterisk, config bit 0
    5, MNU_FLAG, 1,   // '2400 Baud', exit to MSX Baud Rate, asterisk, config bit 1
    5, MNU_FLAG, 2,   // '3600 Baud', exit to MSX Baud Rate, asterisk, config bit 2
    5, MNU_FLAG, 3    // '3670 Baud', exit to MSX Baud Rate, asterisk, config bit 3
  };
  // we need an int8_t, i.e. a signed 8 bit type - (signed) char is OK
  char headPos = 0; // init: top-level menu position
  char menuPos = 1; // init: first sub-menu position
  char menuType = MNU_LIST; // init: we start with a list

  byte oldSettings = getOptions();
  byte newSettings = oldSettings;

  bool updateScreen = true;
  bool keepRunning = true;
  bool bButtonAction = false;

  char menubuf[MAX_MENU_LINE_LEN + 1];

  while (keepRunning) {
    if (updateScreen) {
      // print headline
      printtextF(s_menu[headPos], 0);
      // (sub)menu handling
      switch (menuType) {
        case MNU_LIST: // menu list with options
          if (control[I_PAR1(menuPos)] == MNU_FLAG) {
            strcpy_P(menubuf, s_menu[menuPos]);
            if (bitRead(newSettings, control[I_PAR2(menuPos)])) {
              strcat(menubuf, " *");
            }
            printtext(menubuf, 1);
          } else {
            printtextF(s_menu[menuPos], 1);
          }
          break;
        case MNU_ONOFF: // ON or OFF choice
          strcpy_P(menubuf, menuPos ? PSTR("On ") : PSTR("Off"));
          if (bitRead(newSettings, control[I_PAR2(headPos)]) == menuPos) {
            strcat(menubuf, " *");
          }
          printtext(menubuf, 1);
          break;
        case MNU_FLAG: // Flag choice
          printtextF(PSTR("Unexpected!"), 1); // we should never see this
      }
      updateScreen = false;
    }
    if (bButtonAction) { // we had a keypress and updated the screen, now wait until the key is released
      while (isDownPressed() || isUpPressed() || isPlayPressed() || isStopPressed()) {
        delay(200);
      }
      bButtonAction = false;
    }
    if (isStopPressed()) { // exit sub menu or leave menu
      bButtonAction = true;
      headPos = control[I_BACK(headPos)];
      if (headPos == MNU_EXIT) {
        keepRunning = false;
        continue; // immediately leave while loop
      }
      menuType = control[I_PAR1(headPos)];
      if (menuType > 0) {
        menuPos = menuType; // then the type is the position of the start of the menu
        menuType = 0; // any number >= 0 is a sub-menu, negative numbers are special types
      } else {
        menuPos = 0; // starting with 'off'
      }
      updateScreen = true;
    }
    switch (menuType) {
      case MNU_LIST:
        if (isDownPressed()) {
          bButtonAction = true;
          if (menuPos < control[I_PAR2(headPos)]) {
            menuPos++;
          } else {
            menuPos = control[I_PAR1(headPos)]; // cycle to first item
          }
          updateScreen = true;
        } else if (isUpPressed()) {
          bButtonAction = true;
          if (menuPos > control[I_PAR1(headPos)]) {
            menuPos--;
          } else {
            menuPos = control[I_PAR2(headPos)]; // cycle to last item
          }
          updateScreen = true;
        } else if (isPlayPressed()) { // enter sub menu or toggle flag
          bButtonAction = true;
          char t = control[I_PAR1(menuPos)];
          if (t == MNU_FLAG) {
            // we do not 'enter' a flag menu, we toggle the settings bit value
            newSettings ^= (1 << control[I_PAR2(menuPos)]);
          } else { // we enter another menu level
            headPos = menuPos; // drill down menu
            if (t > 0) {
              menuPos = t;
              menuType = 0;
            } else {
              menuPos = 0;
              menuType = t;
            }
          }
          updateScreen = true;
        }
        break;
      case MNU_ONOFF:
        if (isDownPressed() || isUpPressed()) {
          bButtonAction = true;
          menuPos ^= 1; // toggle between on and off
          updateScreen = true;
        } else if (isPlayPressed()) {
          bButtonAction = true;
          newSettings ^= (1 << control[I_PAR2(headPos)]); // toggle settings bit value
          updateScreen = true;
        }
        break;
      case MNU_FLAG: // should never happen, MNU_LIST handles flags
        break;
    }
  } // while keepRunning

  if (newSettings != oldSettings) {
    setOptions(newSettings); // transfer new settings into marker variables
    saveSettings(); // save settings in persistent storage (EEPROM or file)
  }
} // menuSettings

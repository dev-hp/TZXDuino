#ifndef __platform_h_include
#define __platform_h_include

// included by userconfig.h

#if ( defined(ESP32) || ESP32 )

#define PLATFORM_ODROID_GO  1
#include <odroid_go.h>
#include <SD.h>
// Modified TimerOne variant with ESP32 support, until merged it is here:
// https://github.com/hagen-git/TimerOne
#include <TimerOne.h>

// ESP32 / FreeRTOS method to prevent interrupts via mutex
static portMUX_TYPE waveMutex;
#undef noInterrupts
#undef interrupts
#define noInterrupts()   portENTER_CRITICAL(&waveMutex)
#define interrupts()     portEXIT_CRITICAL(&waveMutex)

#else // (not) ESP32

#define PLATFORM_ARDUINO  1
#include <SdFat.h>
#include <TimerOne.h>

#endif

#endif // __platform_h_include

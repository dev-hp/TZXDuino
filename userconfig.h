#ifndef __userconfig_h_include
#define __userconfig_h_include
/////////////////////////////////////////////////////////////////
/*                                                             */
/*                                                             */
/*          Configure your screen settings here                */
/*    Delete the // of the lines appropriate to your device    */
/*                                                             */
/*                                                             */
/////////////////////////////////////////////////////////////////

#include "platform.h"

// A float datatype is only used in UEFProcessing/UEF_ProcessChunkID/ID0116.
// On an Arduino Uno, enabling this requires additional 514 bytes program space (for the floating point library).
//#define FLOATING_POINT_SUPPORT

#ifdef PLATFORM_ODROID_GO

#define STORAGE_SD_CS_PIN 22      /* ODROID-GO SD card uses IO22 */
#define BUTTONS_ODROID    1       /* ODROID-GO comes with a button library we will use. */
#define AUDIO_OUTPUT_PIN  12      /* ODROID-GO IO12 is pin 3 on the 10-pin connector above the display. */
#define SETTINGS_FILE     1       /* Set this if the settings should be stored in a file on SD CARD. */

#else // not ODROID

// Storage configuration (SD card)
#define STORAGE_SD_CS_PIN 10

// Display configuration
//#define SERIALSCREEN       1      /* Serial output, only for testing and debugging. */

//#define LCDSCREEN16x2      1      /* Set if you are using a 1602 LCD screen with an I2C adapter. */
//#define LCD_I2C_ADDR       0x27   /* Set the I2C address of your 1602 LCD, usually 0x27 or 0x3F. */

//#define RGBLCD             1

#define OLED1306           1      /* Set if you have an SSD1306 OLED screen (0.91" 128x32 or 1.3" 128x64). */
//#define OLED1306_128x64    1      /* Use this line as well if you have a 1.3" OLED with 128x64 (8 lines). */
#define OLED_SPECFONT      1      /* Use this line if you want to use the Spectrum Font by Brendan Alford */

//#define P8544              1      /* Set if you are using an 84x48 PCD8544 screen. */
//#define TZXDuino_Logo      1      /* PCD8544: Set if you want the TZXDuino logo. */


// Buttons
//#define BUTTONS_ARDUINO           /* TZXDuino standard buttons, see Button.h for definitions. */
#define BUTTONS_VMA203            /* Vellemann VMA203 has 5 buttons on one pin (read analog value to determine which one is pressed). */


#define AUDIO_OUTPUT_PIN   9

// Settings (storage) configuration and prototypes
#define SETTINGS_EEPROM    1      /* Set this if the settings should be stored in EEPROM. */
//#define SETTINGS_FILE      1      /* Set this if the settings should be stored in a file on SD CARD. */
#define SETTINGS_ADDRESS   0

#endif // (not) ODROID

// Settings
byte getOptions();
void setOptions(byte settings);
void loadSettings();
void saveSettings();

#endif // __userconfig_h_include

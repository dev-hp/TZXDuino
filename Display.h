#ifndef __display_h_include
#define __display_h_include
// Display abstraction

#define scrollSpeed   250   //Text scroll delay
#define scrollWait    3000  //Delay before scrolling starts
#define LCD_UPDATE_MS 1000  //lcdTime() update interval

void Display_setup();

void printtextF(const char* text, int l);
void printtext(char* text, int l);

// long filename scrolling
void setScrollText(char *text);
void updateScrollText();
void resetScrollPos();

// messages
void msgStarting();
void msgPlaying();
void msgStopped();
void msgSelectFile();
void msgErrFileRead();
void msgErrOverrun();

// counters
void printCounters();
void filePercentPrint();
void tapeCounterReset();
void tapeCounterUpdate();
void tapeCounterPrint();

#endif

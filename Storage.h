#ifndef __storage_h_include
#define __storage_h_include

#define MAXLEN_LONGFILENAME 100           //Maximum length for scrolling filename

char filename[MAXLEN_LONGFILENAME + 1];   //Current filename
unsigned long filesize;

void Storage_setup(void);
void upFile();             // navigation
void downFile();           // navigation
void changeDir();          // navigation
void returnFromSubdir();   // navigation
// void seekFile(int pos); // not required outside Storage.ino
// void getMaxFile();      // not required outside Storage.ino
bool seekPosition(unsigned long p);
int  readBuf(byte *buf, byte len);
bool doesFileExist(char *name);
bool curFileReadOpen(void);
void curFileClose(void);

#endif

#ifndef __buttons_h_include
#define __buttons_h_include

#ifdef BUTTONS_ARDUINO
#define btnPlay       17   //Play Button
#define btnStop       16   //Stop Button
#define btnUp         15   //Up button
#define btnDown       14   //Down button
#define btnRoot       7    //Menu button
#define pinMotor      6    //Motor Sense (connect pin to gnd to play, NC for pause)

// Button query functions, return true if pressed
#define isPlayPressed() (digitalRead(btnPlay) == LOW)
#define isStopPressed() (digitalRead(btnStop) == LOW)
#define isUpPressed()   (digitalRead(btnUp) == LOW)
#define isDownPressed() (digitalRead(btnDown) == LOW)
#define isRootPressed() (digitalRead(btnRoot) == LOW)

// Motor Sense (connect pin to GND to play, NC for pause)
#define isMotorSenseActive() (digitalRead(pinMotor) == LOW)
// if you don't have a Motor Sense wire, set it to active:
//#define isMotorSenseActive() (true)
#endif // BUTTONS_ARDUINO


#ifdef BUTTONS_VMA203
// Buttons of Vellemann VMA203 LCD and Keypad Shield (analog only)
#define btnRoot        1   //SELECT button - Menu
#define btnStop        2   //LEFT button   - Stop
#define btnDown        3   //DOWN button   - Down
#define btnUp          4   //UP button     - Up
#define btnPlay        5   //RIGHT button  - Play

// Button query functions, return true if pressed
#define isPlayPressed() (isVmaButtonPressed(btnPlay))
#define isStopPressed() (isVmaButtonPressed(btnStop))
#define isUpPressed()   (isVmaButtonPressed(btnUp))
#define isDownPressed() (isVmaButtonPressed(btnDown))
#define isRootPressed() (isVmaButtonPressed(btnRoot))

// Motor Sense simulation - always on
#define isMotorSenseActive() (true)

// Prototype of the button-read-function
bool isVmaButtonPressed(byte button);
#endif // BUTTONS_VMA203


#ifdef PLATFORM_ODROID_GO
// Button query functions, return true if pressed
#define isPlayPressed() (GO.BtnA.read()==1)
#define isStopPressed() (GO.BtnB.read()==1)
#define isUpPressed()   (GO.JOY_Y.readAxis()&&(GO.JOY_Y.isAxisPressed()==2))
#define isDownPressed() (GO.JOY_Y.readAxis()&&(GO.JOY_Y.isAxisPressed()==1))
#define isRootPressed() (GO.BtnMenu.read()==1)

// Motor Sense simulation - always on
#define isMotorSenseActive() true

#endif // PLATFORM_ODROID_GO

void Buttons_setup();

#endif

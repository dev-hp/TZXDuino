// Buttons abstraction
#include "userconfig.h"
#include "Buttons.h"

#ifdef BUTTONS_ARDUINO
void Buttons_setup() {
  //General Pin settings
  //Setup buttons with internal pullup
  pinMode(btnPlay, INPUT_PULLUP);
  digitalWrite(btnPlay, HIGH);
  pinMode(btnStop, INPUT_PULLUP);
  digitalWrite(btnStop, HIGH);
  pinMode(btnUp, INPUT_PULLUP);
  digitalWrite(btnUp, HIGH);
  pinMode(btnDown, INPUT_PULLUP);
  digitalWrite(btnDown, HIGH);
  pinMode(pinMotor, INPUT_PULLUP);
  digitalWrite(pinMotor, HIGH);
  pinMode(btnRoot, INPUT_PULLUP);
  digitalWrite(btnRoot, HIGH);
}
#endif // BUTTONS_ARDUINO

#ifdef BUTTONS_VMA203
void Buttons_setup() {
  // nothing required
}

bool isVmaButtonPressed(byte button) {
  // Vellemann VMA203 LCD/Button shield has an analog resistor ladder with buttons
  int analogKeyVoltage = analogRead(0);

  if (analogKeyVoltage > 1000)
    return false; // no button pressed

  // we try not to read the state and store which button is pressed,
  // we look for a specific button to be pressed (or not)
  // NB: analog values match _my_ inputs, you need to test this for your hardware
  switch (button) {
    case btnRoot: // SELECT - 633
      return (analogKeyVoltage > 580 && analogKeyVoltage < 700);
    case btnStop: // LEFT - 403
      return (analogKeyVoltage > 360 && analogKeyVoltage < 450);
    case btnDown: // DOWN - 251
      return (analogKeyVoltage > 210 && analogKeyVoltage < 340);
    case btnUp: // UP - 99
      return (analogKeyVoltage > 60 && analogKeyVoltage < 140);
    case btnPlay: // RIGHT - 1
      return (analogKeyVoltage > 0 && analogKeyVoltage < 50);
  }
  return false; // unknown button number, or value between 850 and 1000
}
#endif // BUTTONS_VMA203


#ifdef BUTTONS_ODROID
void Buttons_setup() {
  // nothing required when using ODROID-GO library
}
#endif
